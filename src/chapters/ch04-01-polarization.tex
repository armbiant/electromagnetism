\section{Polarization}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dielectrics}
We shall study \textbf{electric fields in matter}.
By matter, we mean solids, liquids, gasses, metal, etc.
These substances do not all respond in the same way to electrostatic fields.

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-conductor_vs_dielectric-01}
    \caption{Conductor vs. Dielectric.}%
\end{figure}
\textbf{Two large classes of matter:}
\begin{enumerate}
    \item Conductors: ``Unlimited'' supply of charge that are free to move about through the material.
    \item Insulators (or \textit{dielectrics}): All charges are bound to specific atoms or molecules.
        All they can do is move a bit within the atom or molecule.
\end{enumerate}

The cumulative effects of such microscopic displacements accounts for the characteristic behavior of dielectric materials.

Two principle mechanisms by which electric fields can distort the charge distribution of a dielectric atom or molecule: \textit{Stretching and rotating}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Induced Dipoles}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-induced_dipole_schematic-01}
    \caption{Illustration of an induced dipole.}%
\end{figure}
\begin{itemize}
    \item Although the atom as a whole is neutral, there is a positively charged core (the nucleus) and negatively charged electron cloud surrounding it.
    \item Therefore, the nucleus is pushed in the direction of the field, and the electron the opposite way.
    \item Such an atom is said to be \textbf{polarized}:
        The atom obtains a tiny dipole moment $\mathbf p$, which points in the same direction as $\mathbf E$.
    \item Typically, this induced dipole is approximately proportional to the field
        \begin{equation}
            \mathbf p = \alpha \mathbf E
        \end{equation}
\end{itemize}

\framed{%
    \example{4.1}%
    A primitive model for an atom consists of a point nucleus ($+q$) surrounded by a uniformly charged spherical cloud ($-q$) of radius $a$.
    Calculate the atomic porizability of such an atom.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-example_1-01}
    \caption{Example 4.1 Polarizability of an atom.}%
\end{figure}
\begin{itemize}
    \item With an external field $\mathbf E$, the nucleus will be shifted slightly with respect to the electron cloud.
    \item The off-centered nucleus will be attracted due to an additional electric force which will become as strong as the force due to the external field.
\end{itemize}

Let $E_e$ be the internal field due to the electron cloud.
Then the fields at a distance $d$ from the center is
\[
    \begin{aligned}
    E_e
    &=
    \frac{1}{4\pi\epsilon_0} \frac{1}{d^2} \left( \rho_e \frac{4}{3} \pi d^3 \right)
    \\
    &\qquad\qquad \color{blue}\leftarrow \rho_e = -q (4/3) \pi a^3
    \\
    &=
    -\frac{1}{4\pi\epsilon_0} \frac{qd}{a^3}
    \end{aligned}
\]
At equilibrium ($E + E_e = 0$), then,
\[
    E = \frac{1}{4\pi\epsilon_0} \frac{qd}{a^3}
    \quad \rightarrow \quad
    p = qd = (4\pi\epsilon_0 a^3) E
\]
Therefore,
\begin{equation}
    \alpha = 4\pi\epsilon_0 a^3
\end{equation}

\framed{%
    Homework problems: 4.1, 4.2, 4.4
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Alignment of Polar Molecules}
\framed{%
    Some molecules have built-in permanent dipole moments, e.g., water molecule.
}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-water_molecule-01}
    \caption{Water molecule and torque.}%
\end{figure}

\textbf{Q.} What happens when such molecules (called polar molecules) are placed in an electric field?

\subsubsection{Uniform field}
If the field is \textit{uniform} the force on the positive end, $\mathbf F_+ = q\mathbf E$, exactly \textit{cancels} the force on the negative end, $\mathbf F_- = -q\mathbf E$.

But there will be a torque:
\[
    \begin{aligned}
        \mathbf N
        &=
        \mathbf r_+ \times \mathbf F_+
        +
        \mathbf r_- \times \mathbf F_-
        \\
        &=
        \left( \frac{1}{2} \mathbf d \right) \times (q\mathbf E)
        +
        \left(-\frac{1}{2} \mathbf d \right) \times (-q\mathbf E)
        \\
        &=
        \frac{1}{2} (q\mathbf d) \times \mathbf E
        +
        \frac{1}{2} (q\mathbf d) \times \mathbf E
        \\
        &=
        (q\mathbf d) \times \mathbf E
    \end{aligned}
\]
or
\begin{equation}
    \boxed{%
        \mathbf N = \mathbf p \times \mathbf E
    }
    \label{eq:ch04:TorqueOnPolarMoleculeInUniformField}
\end{equation}
Note that $\mathbf N$ is in such a direction as to line $\mathbf p$ up with $\mathbf E$.

\subsubsection{Nonuniform field}
If the field is \textit{nonuniform} so that $\mathbf F_+$ does not exactly balance $\mathbf F_-$, there will be a net force on the dipole, in addition to the torque.

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-net_force_in_nonuniform_field-01}
    \caption{Net force on a polar molecule in a nonuniform field.}%
\end{figure}
The force on a dipole in a nonuniform field is
\[
    \mathbf F = \mathbf F_+ + \mathbf F_-
    = q (\mathbf E_+ - \mathbf E_-) = q (\Delta \mathbf E)
\]
where $\Delta \mathbf E \equiv \mathbf E_+ - \mathbf E_-$ represents the difference between the field at $+q$ and $-q$.
In components,
\[
    \Delta\mathbf E
    =
    \Delta E_x \uvec x
    +
    \Delta E_y \uvec y
    +
    \Delta E_z \uvec z
\]

When $|\mathbf d| \ll |\mathbf r|$, one can approximate $\Delta \mathbf E$ (i.e., Taylor's expansion):
\[
    \Delta E_x \approx \mathbf d \cdot \nabla E_x
    ,\quad
    \Delta E_y \approx \mathbf d \cdot \nabla E_y
    ,\quad
    \Delta E_z \approx \mathbf d \cdot \nabla E_z
\]
or in vector form,
\[
    \Delta\mathbf E = (\mathbf d \cdot \nabla) \mathbf E
\]
Therefore,
\begin{equation}
    \boxed{%
        \mathbf F = (\mathbf p \cdot \nabla) \mathbf E
    }
    \label{eq:ch04:NetForceOnPolarMoleculeInNonuniformField}
\end{equation}

For a ``perfect'' dipole of infinitesimal length
\[
    \mathbf N = \mathbf p \times \mathbf E
\]
gives the torque \textit{about the center of the dipole} even in a \textit{nonuniform} field.

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-torque_in_nonuniform_field-01}
    \caption{Torque on a polar molecule in a nonuniform field.}%
\end{figure}
About any \textit{other} point, however, the torque is given by
\[
    \begin{aligned}
        \mathbf N
        &=
        \mathbf r_+ \times \mathbf F_+
        +
        \mathbf r_- \times \mathbf F_-
        \\
        &=
        \mathbf r_+ \times (q\mathbf E_+)
        -
        \mathbf r_- \times (q\mathbf E_-)
        \\
        &=
        \left(\mathbf r + \frac{1}{2} \mathbf d \right) \times (q\mathbf E_+)
        -
        \left(\mathbf r - \frac{1}{2} \mathbf d \right) \times (q\mathbf E_-)
        \\
        &=
        \mathbf r \times \underbrace{\left( \mathbf F_+ + \mathbf F_- \right)}_{\color{blue}=\mathbf F}
        +
        \mathbf p \times \left( \frac{\mathbf E_+ + \mathbf E_-}{2} \right)
    \end{aligned}
\]
Defining the field at the center of the dipole is
\[
    \mathbf E \simeq \frac{\mathbf E_+ + \mathbf E_-}{2}
\]
it follows
\begin{equation}
    \mathbf N = \mathbf r \times \mathbf F + \mathbf p \times \mathbf E
\end{equation}

\framed{%
    Homework problems: 4.5, 4.6, 4.7, 4.9
}

\framed{%
    \problem{4.6}%
    A (perfect) dipole $\mathbf p$ is situated a distance $z$ above an infinite grounded conducting plane.
    The dipole makes an angle $\theta$ with the perpendicular to the plane.
    Find the torque on $\mathbf p$.
    If the dipole is free to rotate, in what orientation will it come to rest?
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-problem_6-01}
    \caption{Problem 4.6 Configuration.}%
    \label{fig:ch04:Problem6}
\end{figure}
Let's solve an analogous problem by placing an image dipole $\mathbf p_i$ at $-z \uvec z$ (Figure~\ref{fig:ch04:Problem6}).
The potential above the plane is given by
\[
    V(\mathbf r)
    =
    \frac{1}{4\pi\epsilon_0}
    \left(
        \frac{\mathbf p \cdot \spvec}{\eta^3}
        +
        \frac{\mathbf p_i \cdot \spvec_i}{\eta_i^3}
    \right)
\]
where $\spvec$ and $\spvec_i$ are the separation vectors from $\mathbf p$ and $\mathbf p_i$, respectively.

The image dipole moment $\mathbf p_i$ must be determined so that the potential at the plane becomes zero.
Without loss of generality, we can choose a coordinate system where $\mathbf p$ is contained in the $xz$ plane, that is,
\[
    \mathbf p
    =
    p_x \uvec x + p_z \uvec z
    =
    p (\sin\theta \uvec x + \cos\theta \uvec z)
\]
By symmetry, $\mathbf p_i$ should also be contained in the $xz$ plane.
Since the potential vanishes at point (1), it follows
\[
    0
    =
    \mathbf p \cdot \spvec
    +
    \mathbf p_i \cdot \spvec_i
    =
    (-p_z z)
    +
    (p_{i,z} z)
    \quad\rightarrow\quad
    p_{i,z} = p_z
\]
where we made use of $\spvec = -z\uvec z$ and $\spvec_i = z\uvec z$.
Similarly, at point (2), we have $\spvec = x\uvec x - z\uvec z$ and $\spvec_i = x\uvec x + z\uvec z$, and thus
\[
    0
    =
    (p_x x - p_z z) + (p_{i,x} x + p_{i,z} z)
    =
    (p_x + p_{i,x})x + \underbrace{(p_{i,z} - p_z)}_{\color{blue}=0}z
    \quad\rightarrow\quad
    p_{i,x} = -p_x
\]
Therefore, we find that
\[
    \mathbf p_i = -p_x \uvec x + p_z \uvec z
\]

Using Eq. (3.104), the electric field $\mathbf E_i$ due to $\mathbf p_i$ at the location of $\mathbf p$ is given by
\[
    \begin{aligned}
        \mathbf E_i
        &=
        \frac{1}{4\pi\epsilon_0 (2z)^3}
        \left( 3 (\mathbf p_i \cdot \uvec z)\uvec z - \mathbf p_i \right)
        \\
        &=
        \frac{1}{4\pi\epsilon_0 (2z)^3}
        \left( 3 p_z \uvec z - (-p_x\uvec x + p_z\uvec z) \right)
        \\
        &=
        \frac{1}{4\pi\epsilon_0 (2z)^3}
        \left( p_z \uvec z + \mathbf p \right)
    \end{aligned}
\]
The torque is then
\[
    \mathbf N
    =
    \mathbf p \times \mathbf E_i
    =
    \frac{p_z}{4\pi\epsilon_0 (2z)^3}
    \left( \mathbf p \times \uvec z \right)
    =
    \frac{p \cos\theta}{4\pi\epsilon_0 (2z)^3}
    \left( p_x\uvec x \times \uvec z \right)
    =
    \frac{p^2 \cos\theta\sin\theta}{4\pi\epsilon_0 (2z)^3} (-\uvec y)
\]
Or,
\[
    \mathbf N
    =
    \frac{p^2 \sin 2\theta}{8\pi\epsilon_0 (2z)^3} (-\uvec y)
\]
Note that the direction of $-\uvec y$ is out of plane.

The torque vanishes when $\theta = 0$, $\pm\pi/2$, and $\pi$.
However, $\theta = \pm\pi/2$ is an unstable orientation, so the dipole will come to a rest at $\theta = 0$ or $\pi$ (when there is a friction at the pivot).
Using the result of Problem 4.7, we can write the energy of the dipole configuration
\[
    \begin{aligned}
        U
        &=
        -\mathbf p \cdot \mathbf E
        \\
        &=
        -
        \frac{1}{4\pi\epsilon_0 (2z)^3}
        \mathbf p \cdot \left( p_z \uvec z + \mathbf p \right)
        \\
        &=
        -
        \frac{1}{4\pi\epsilon_0 (2z)^3}
        (p_z^2 + p^2)
        \\
        &=
        -
        \frac{p^2}{4\pi\epsilon_0 (2z)^3}
        (\cos\theta^2 + 1)
        \\
        &=
        -
        \frac{p^2}{4\pi\epsilon_0 (2z)^3}
        \frac{\cos 2\theta + 3}{2}
    \end{aligned}
\]
In fact, energy becomes maximum when $\theta = \pm\pi/2$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Polarization}
\framed{%
    \textbf Q. What happens to a piece of dielectric material when it is placed in an electric field?

    \begin{itemize}
        \item The field will induce tiny dipole moments inside the dielectric, pointing in the same direction as the field.
        \item Or, in the case of polar molecules, each permanent dipole will experience a torque, tending to line it up along the field direction.
    \end{itemize}
    As a result, the material becomes \textbf{polarized}.
}

A convenient measure of this effect is
\[
    \mathbf P
    =
    \text{dipole moment per unit volume}
\]
which is called \textbf{polarization}.\\

Let's forget for a moment about the cause of the polarization, and instead study the field that a chunk of polarized material itself produces.
