\section{Laplace's Equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Introduction}
\textbf{Goal in electrostatics}: To find the electric field $\mathbf E$ of a stationary charge distribution $\rho$.\\

Three ways to find $\mathbf E$ we learned so far:
\begin{enumerate}
    \item Coulomb's law
        \[
            \mathbf E = \frac{1}{4\pi\epsilon_0} \int \rho(\mathbf r') \frac{\uspvec}{\eta^2} d\tau'
        \]
        \textit{\underline{Pros}}. Direct way to find $\mathbf E$.\\
        \textit{\underline{Cons}}. Integral is difficult in all but few simple cases.

    \item Gauss's law: The simplest way to find $\mathbf E$ if the system has symmetry. (Otherwise, not very useful in practice.)

    \item Gradient of potential
        \[
            \mathbf E = -\nabla V
            \quad\leftarrow\quad
            V(\mathbf r)
            =
            \frac{1}{4\pi\epsilon_0} \int \frac{\rho(\mathbf r')}{\eta} d\tau'
        \]
        \textit{\underline{Pros}}. Easier than Coulomb's law.\\
        \textit{\underline{Cons}}. Still, integral can be tough and $\rho$ may not be known for conductors.\\
\end{enumerate}

The fourth way to find $\mathbf E$ is to solve Poisson's equation
\[
    \nabla^2 V = -\frac{\rho}{\epsilon_0}
\]
together with appropriate boundary conditions.
In a region where there is no charge ($\rho = 0$), Poisson's equation becomes Laplace's equation
\[
    \nabla^2 V = 0
\]

In Cartesian coordinates, Laplace's equation reads
\[
    \frac{\partial^2 V}{\partial x^2}
    +
    \frac{\partial^2 V}{\partial y^2}
    +
    \frac{\partial^2 V}{\partial z^2}
    =
    0
\]

Solution to Laplace's equation is called \textit{harmonic function}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setcounter{subsection}{4}
\subsection{Boundary Conditions and Uniqueness Theorems}
\subsubsection{I\@. First uniqueness theorem}
\framed{%
    The solution to Laplace's equation in some volume $\mathcal V$ is uniquely determined if $V$ is specified on the boundary surface $\mathcal S$.
}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch03-uniqueness_I-01}
    \caption{A region of empty space enclosed by a surface of the prescribed potential.}%
\end{figure}
\textbi{Proof.}
Suppose that there are \textit{two} solutions to Laplace's equation, namely, $V_1$ and $V_2$, which must satisfy
\[
    \nabla^2 V_1 = 0 \quad \text{and} \quad \nabla^2 V_2 = 0
\]
The two solutions must have the same potential values at the surface:
\[
    V_1(\mathcal S) = V_2(\mathcal S)
\]
Since Laplace's equation is linear, $V_3 = V_1 - V_2$ is also a solution:
\[
    \nabla^2 V_3
    =
    \nabla^2 V_1
    -
    \nabla^2 V_2
    =
    0
\]
Also, $V_3$ mush vanish at the boundary:
\[
    V_3(\mathcal S) = V_1(\mathcal S) - V_2(\mathcal S) = 0
\]
Since Laplace's equation allows no local maxima or minima, $V_3$ must be zero everywhere, so
\[
    V_1 = V_2
\]
This means that the solution is uniquely defined.

\framed{%
    \textbf{Corollary:}
    The potential in a volume is uniquely determined if
    \begin{enumerate}
        \item [(a)] the charge density throughout the region, and
        \item [(b)] the value of $V$ on all boundaries,
    \end{enumerate}
    are specified.
}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch03-uniqueness_I-02}
    \caption{A region of finite charge surrounded by a surface of the prescribed potential.}%
\end{figure}
Let's throw some charge inside, in which case $V$ obeys Poisson's equation.
Proceeding with the same argument,
\[
    \nabla^2 V_1 = -\frac{\rho}{\epsilon_0}
    \quad \text{and} \quad
    \nabla^2 V_2 = -\frac{\rho}{\epsilon_0}
\]
and $V_1(\mathcal S) = V_2(\mathcal S)$.
The difference, $V_3 = V_1 - V_2$, satisfies Laplace's equation:
\[
    \nabla^2 V_3
    =
    \nabla^2 V_1 - \nabla^2 V_2
    =
    -\frac{\rho}{\epsilon_0}
    +\frac{\rho}{\epsilon_0}
    =
    0
\]
Since $V_3 = 0$ at the surface, but no local minima or maxima is allowed, $V_3$ (which is the solution of Laplace's equation) mush be zero everywhere, hence
\[
    V_1 = V_2
\]

\subsubsection{II\@. Second uniqueness theorem}
\framed{%
    In a volume $\mathcal V$ surrounded by conductors and containing a specified charge $q$, the electric field is uniquely determined if the total charge on each conductor is given.
}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch03-uniqueness_II-01}
    \caption{A region of some charge density which embedded in it contains several conductors.}%
\end{figure}
\textbi{Proof}
Suppose that two fields, $\mathbf E_1$ and $\mathbf E_2$, satisfy the conditions of the problem.
Both must obey Gauss's law inside:
\[
    \nabla \cdot \mathbf E_1
    =
    \frac{\rho}{\epsilon_0}
    \quad \text{and} \quad
    \nabla \cdot \mathbf E_2
    =
    \frac{\rho}{\epsilon_0}
\]
Gauss's law at all conducting surfaces ($\mathcal S_i$):
\[
    \oint_{\mathcal S_i}
    \mathbf E_1 \cdot d\mathbf a
    =
    \frac{Q_i}{\epsilon_0}
    \quad \text{and} \quad
    \oint_{\mathcal S_i}
    \mathbf E_2 \cdot d\mathbf a
    =
    \frac{Q_i}{\epsilon_0}
\]
And Gauss's law for the outer boundary (this could be at infinity):
\[
    \underset{\text{outer boundary}}{\oint}
    \mathbf E_1 \cdot d\mathbf a
    =
    \frac{Q_{\rm tot}}{\epsilon_0}
    \quad \text{and} \quad
    \underset{\text{outer boundary}}{\oint}
    \mathbf E_2 \cdot d\mathbf a
    =
    \frac{Q_{\rm tot}}{\epsilon_0}
\]
The difference $\mathbf E_3 = \mathbf E_1 - \mathbf E_2$ obeys
\[
    \nabla \cdot \mathbf E_3
    =
    \nabla \cdot \mathbf E_1
    -
    \nabla \cdot \mathbf E_2
    =
    0
\]
inside, and
\[
    \oint \mathbf E_3 \cdot d\mathbf a
    =
    \oint \mathbf E_1 \cdot d\mathbf a
    -
    \oint \mathbf E_2 \cdot d\mathbf a
    =
    0
\]
over each boundary surface.

We exploit the potential inside a conductor is constant, hence $V_3 = \mathrm{const}$ (not necessarily zero).
We then calculate
\[
    \nabla \cdot (V_3 \mathbf E_3)
    =
    V_3 \underbrace{(\nabla \cdot \mathbf E_3)}_{\color{blue}=0}
    +
    \mathbf E_3 \cdot (\nabla V_3)
    =
    -E_3^2
\]
Integrating over the volume inside and making use of divergence theorem,
\[
    \int_{\mathcal V}
    \nabla \cdot ({\color{blue} V_3} \mathbf E_3) d\tau
    =
    \oint_{\mathcal S} {\color{blue} V_3} \mathbf E_3 \cdot d\mathbf a
    =
    -\int_{\mathcal V} {(E_3)}^2 d\tau
\]
where $\mathcal S$ covers all boundaries of the region in question---the conductors and outer boundary.
Since $V_3$ is constant over each surface, we pull it out of the integral, and what remains is
\[
    \int_{\mathcal V} {(E_3)}^2 d\tau
    =
    -{\color{blue} V_3} \underbrace{\oint_{\mathcal S} \mathbf E_3 \cdot d\mathbf a}_{\color{red}=0}
    =
    0
\]
Since ${(E_3)}^2 \ge 0$, the only way the integral can vanish is $E_3 = 0$ everywhere, hence
\[
    \mathbf E_1 = \mathbf E_2
\]
