\section{The Auxiliary Field $\mathbf H$}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Amp\`ere's Law in Magnetized Materials}
\begin{itemize}
    \item The field due to magnetization of the medium is just the field produced by the bound currents, $\mathbf J_b = \nabla \times \mathbf M$ and $\mathbf K_b = \mathbf M \times \uvec n$.

    \item The total field will be then the field attributable to bound currents \textit{plus} the field due to everything else, the latter of which we call the \textbf{free current} $\mathbf J_f$.

    \item The free current involves actual transport of charge, whereas the bound current is there because of magnetization.
\end{itemize}

The total current can be written as
\begin{equation}
    \mathbf J = \mathbf J_f + \mathbf J_b
\end{equation}
The Amp\`ere's law then reads
\[
    \frac{1}{\mu_0} (\nabla \times \mathbf B)
    =
    \mathbf J = \mathbf J_f + \mathbf J_b
    =
    \mathbf J_f + \nabla \times \mathbf M
\]
Collecting the curls,
\[
    \nabla \times
    \underbrace{\left( \frac{1}{\mu_0}\mathbf B - \mathbf M \right)}_{\color{blue}\mathbf H}
    =
    \mathbf J_f
\]
The quantity in the parentheses (called the \textbf{auxiliary field}) is designated by the letter $\mathbf H$
\begin{equation}
    \boxed{%
        \mathbf H = \frac{1}{\mu_0}\mathbf B - \mathbf M
    }
\end{equation}
Therefore, in terms of $\mathbf H$, the Amp\`ere's law reads
\begin{equation}
    \boxed{%
        \nabla \times \mathbf H = \mathbf J_f
    }
    \label{eq:ch06:AmpereLaw:Differential}
\end{equation}
or in integral form
\begin{equation}
    \oint \mathbf H \cdot d\mathbf l = I_{f_{\rm enc}}
    \label{eq:ch06:AmpereLaw:Integral}
\end{equation}
where $I_{f_{\rm enc}}$ is the total free current passing through the Amperian loop.

\begin{itemize}
    \item Just like $\mathbf D$ in electrostatics, $\mathbf H$ permits us to express Amp\`ere's law in terms of the free current alone.

    \item Particularly, when symmetry permits, we can calculate $\mathbf H$ immediately from Eq.~(\ref{eq:ch06:AmpereLaw:Integral}) by the usual Amp\`ere's law methods.
\end{itemize}

\framed{%
    \example{6.2}%
    A long copper rod of radius $R$ carries a uniformly distributed (free) current $I$.
    Find $\mathbf H$ inside and outside the rod.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch06-example_2-01}
    \caption{Example 6.2 Auxiliary field of a long copper rod.}%
\end{figure}
Copper is weakly \textit{diamagnetic}, so dipoles line up opposite to the field.
This results in a bound current running \textit{antiparallel} to $I$ within the wire, thereby reducing the total current.

But we do not need to know the exact details of magnetization of the material if we are concerned with $\mathbf H$.
Applying Eq.~(\ref{eq:ch06:AmpereLaw:Integral}) to an Amperian loop of radius $s < R$ (i.e., inside)
\[
    H (2\pi s) = I_{f_{\rm enc}} = \frac{I}{\pi R^2} \cdot \pi s^2
\]
So,
\[
    \mathbf H = \frac{I}{2\pi R^2} s \uvec \phi
    ,\qquad s \le R
\]
Outside the wire,
\[
    \mathbf H = \frac{I}{2\pi s} \uvec \phi
    ,\qquad s \ge R
\]

In the latter region, $\mathbf M = 0$, so
\[
    \mathbf B = \mu_0 \mathbf H = \frac{\mu_0 I}{2\pi s} \uvec \phi
    ,\qquad s \ge R
\]
However, $\mathbf B$ inside the wire cannot be determined, since we have no way of knowing $\mathbf M$.

\separator%

As a side note, $\mathbf H$ is a more useful quantity than $\mathbf D$:
\begin{itemize}
    \item In labs, to build an electromagnet you run a certain (free) current through a coil.
        The current is the thing you read on the dial (i.e., Ammeter), and this determines $\mathbf H$.

        $\mathbf B$ depends on the specific material you used.

    \item For the case of $\mathbf D$, consider a parallel-plate capacitor.
        You connect the plates to a battery of known voltage.
        It's the potential difference (i.e., voltage) that you read on the dial, not the (free) charge.
        The potential difference determines $\mathbf E$.

        $\mathbf D$ depends on the details of the dielectric you used.

    \item Many authors call $\mathbf H$, not $\mathbf B$, the ``magnetic field,'' and $\mathbf B$ the ``flux density,'' or magnetic ``induction.''
\end{itemize}

\framed{%
    Homework problems: 6.12, 6.13a
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{A Deceptive Parallel}
Like $\mathbf D$ in the electrostatics, Eq.~(\ref{eq:ch06:AmpereLaw:Differential}) alone does not determines $\mathbf H$; you must also know the divergence which is not always zero:
\begin{equation}
    \nabla \cdot \mathbf H
    =
    \frac{1}{\mu_0} \underbrace{\nabla \cdot \mathbf B}_{\color{blue}=0} - \nabla \cdot \mathbf M
    =
    -\nabla \cdot \mathbf M \ {\color{blue} \ne\,0}
\end{equation}
Only when the divergence of $\mathbf M$ vanishes is the parallel between $\mathbf B$ and $\mu_0 \mathbf H$ faithful.
(This is the case if there is the symmetry to exploit the Amp\`ere's methods.)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Boundary Conditions}
The magnetostatic boundary conditions can be rewritten in terms of $\mathbf H$ and the free current:
\begin{equation}
    H_{\rm above}^\perp - H_{\rm below}^\perp = -(M_{\rm above}^\perp - M_{\rm below}^\perp)
\end{equation}
and
\begin{equation}
    \mathbf H_{\rm above}^\| - \mathbf H_{\rm below}^\| = \mathbf K_f \times \uvec n
\end{equation}

\framed{%
    Homework problems: 6.15
}
