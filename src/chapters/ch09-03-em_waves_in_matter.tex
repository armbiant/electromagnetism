\section{Electromagnetic Waves in Matter}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Propagation in Linear Media}
Inside matter, but in regions where there is no free charge or free current ($\rho_f = 0$ and $\mathbf J_f = 0$), Maxwell's equations becomes
\begin{equation}
    \left.
        \begin{array}{rlcrl}
            \text{(i)}   & \displaystyle \nabla \cdot \mathbf D = 0, &&
            \text{(iii)} & \displaystyle \nabla \times \mathbf E = -\frac{\partial\mathbf B}{\partial t}
            \\\\
            \text{(ii)}  & \displaystyle \nabla \cdot \mathbf B = 0, &&
            \text{(iv)}  & \displaystyle \nabla \times \mathbf H = \phantom{-}\frac{\partial\mathbf D}{\partial t}
        \end{array}
    \right\}
\end{equation}
If the medium is linear,
\begin{equation}
    \mathbf D = \epsilon \mathbf E
    ,\qquad
    \mathbf H = \frac{1}{\mu} \mathbf B
\end{equation}
and homogeneous ($\nabla \epsilon = \nabla \mu = 0$), they reduce to
\begin{equation}
    \left.
        \begin{array}{rlcrl}
            \text{(i)}   & \displaystyle \nabla \cdot \mathbf E = 0, &&
            \text{(iii)} & \displaystyle \nabla \times \mathbf E = -\frac{\partial\mathbf B}{\partial t}
            \\\\
            \text{(ii)}  & \displaystyle \nabla \cdot \mathbf B = 0, &&
            \text{(iv)}  & \displaystyle \nabla \times \mathbf B = \mu\epsilon\frac{\partial\mathbf E}{\partial t}
        \end{array}
    \right\}
\end{equation}
Comparing these equations with those from free space, it is evident that electromagnetic waves propagate through a linear homogeneous medium at a speed
\begin{equation}
    v = \frac{1}{\sqrt{\mu\epsilon}} = \frac{c}{n}
\end{equation}
where
\begin{equation}
    n = \sqrt{\frac{\epsilon\mu}{\epsilon_0\mu_0}}
\end{equation}
is the \textbf{index of refraction} of the substance.
For most materials, $\mu \simeq \mu_0$, so
\begin{equation}
    n \simeq \sqrt{\epsilon_r}
\end{equation}
where $\epsilon_r = \epsilon/\epsilon_0$ is the dielectric constant.
Since $\epsilon_r > 1$, almost always, light travels more slowly through matter.\\

All of the previous results carry over with the simple replacement: $\epsilon_0 \rightarrow \epsilon$, $\mu_0 \rightarrow \mu$, and $c \rightarrow v$.
For monochromatic plane waves, $\omega = k v$ and $B_0 = E_0/v$.

\begin{itemize}
    \item The energy density:
        \begin{equation}
            u = \frac{1}{2} \left( \epsilon E^2 + \frac{1}{\mu} B^2 \right)
            = \epsilon E^2
        \end{equation}

    \item Poynting vector (propagation in the $z$ direction):
        \begin{equation}
            \mathbf S = \frac{1}{\mu} (\mathbf E \times \mathbf B)
            = u v \uvec z
        \end{equation}

    \item Intensity:
        \begin{equation}
            I = \frac{1}{2} \epsilon v E_0^2
        \end{equation}

    \item etc.
        \\
\end{itemize}

\framed{%
    \textbf Q. What happens when a wave passes from one medium to another (e.g., from air to water)?
}

Recall the boundary conditions we derived in Chapter 7:
\begin{equation}
    \left.
        \begin{array}{rlcrl}
            \text{(i)}   & \displaystyle \epsilon_1 E_1^\perp = \epsilon_2 E_2^\perp, &&
            \text{(iii)} & \displaystyle \mathbf E_1^\| = \mathbf E_2^\|
            \\\\
            \text{(ii)}  & \displaystyle \epsilon_1 B_1^\perp = \epsilon_2 B_2^\perp, &&
            \text{(iv)}  & \displaystyle \frac{1}{\mu_1} \mathbf B_1^\| = \frac{1}{\mu_2} \mathbf B_2^\|
        \end{array}
    \right\}
\end{equation}
which relate the electric and magnetic fields at the interface.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Reflection and Transmission at Normal Incidence}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch09-reflection_transmission-01}
    \caption{Field vectors of incident, reflected, and transmitted waves at the two sides of the interface.}%
\end{figure}
Suppose the $xy$ plane forms the boundary between two linear media.
A plane wave of frequency $\omega$, traveling in the $z$ direction and polarized in the $x$ direction approaches the interface from the left:
\begin{equation}
    \left.
        \begin{aligned}
            \cxvec E_I(z, t) &= \cxvec E_{0_I} e^{i(k_1 z - \omega t)} \uvec x \\
            \cxvec B_I(z, t) &= \frac{1}{v_1} \cxvec E_{0_I} e^{i(k_1 z - \omega t)} \uvec y \\
        \end{aligned}
    \right\}
\end{equation}
It gives rise to a reflected wave
\begin{equation}
    \left.
        \begin{aligned}
            \cxvec E_R(z, t) &= \cxvec E_{0_R} e^{i(-k_1 z - \omega t)} \uvec x \\
            \cxvec B_R(z, t) &= -\frac{1}{v_1} \cxvec E_{0_R} e^{i(-k_1 z - \omega t)} \uvec y \\
        \end{aligned}
    \right\}
\end{equation}
traveling back to the left (the minus sign in $\cxvec B_R$ is required by Faraday's law), and a transmitted wave
\begin{equation}
    \left.
        \begin{aligned}
            \cxvec E_T(z, t) &= \cxvec E_{0_T} e^{i(k_2 z - \omega t)} \uvec x \\
            \cxvec B_T(z, t) &= \frac{1}{v_2} \cxvec E_{0_T} e^{i(k_2 z - \omega t)} \uvec y \\
        \end{aligned}
    \right\}
\end{equation}
which continues on to the right.

At the interface ($z = 0$), the combined fields on the left, $\cxvec E_I + \cxvec E_R$ and $\cxvec B_I + \cxvec B_R$, must join the field on the right, $\cxvec E_T$ and $\cxvec B_T$, in accordance with the boundary conditions.

Conditions (i) and (ii) become trivial ($E_1^\perp = E_2^\perp = 0$ and $B_1^\perp = B_2^\perp = 0$), because there is no components perpendicular to the surface.

Condition (iii) requires that
\begin{equation}
    \tilde E_{0_I} + \tilde E_{0_R} = \tilde E_{0_T}
    \label{eq:ch09:03:ConditionIII}
\end{equation}
and condition (iv) says
\[
    \frac{1}{\mu_1} \left( \frac{1}{v_1} \tilde E_{0_I} - \frac{1}{v_1} \tilde E_{0_R} \right)
    =
    \frac{1}{\mu_2} \left( \frac{1}{v_2} \tilde E_{0_T} \right)
\]
or
\begin{equation}
    \tilde E_{0_I} - \tilde E_{0_R} = \beta \tilde E_{0_T}
    \label{eq:ch09:03:ConditionIV}
\end{equation}
where
\begin{equation}
    \beta \equiv \frac{\mu_1 v_1}{\mu_2 v_2} = \frac{\mu_1 n_2}{\mu_2 n_1}
\end{equation}
Solving Eqs.~(\ref{eq:ch09:03:ConditionIII}) and (\ref{eq:ch09:03:ConditionIV}) for $\tilde E_{0_R}$ and $\tilde E_{0_T}$,
\begin{equation}
    \tilde E_{0_R} = \left( \frac{1 - \beta}{1 + \beta} \right) \tilde E_{0_I}
    ,\quad
    \tilde E_{0_T} = \left( \frac{2}{1 + \beta} \right) \tilde E_{0_I}
    \label{eq:ch09:CXAmplitudeOfNormalIncidence}
\end{equation}
If $\mu \simeq \mu_0$ (for most substances), then $\beta = v_1/v_2$ and
\begin{equation}
    \tilde E_{0_R} = \left( \frac{v_2 - v_1}{v_2 + v_1} \right) \tilde E_{0_I}
    ,\quad
    \tilde E_{0_T} = \left( \frac{2v_2}{v_2 + v_1} \right) \tilde E_{0_I}
\end{equation}
which are identical to Eqs.~(\ref{eq:ch09:ReflectTransmitAmplitudes}).
As in the case of string wave, if $v_2 > v_1$, the reflected wave is in phase, and if $v_2 < v_1$, it is out of phase.

The real amplitudes are
\begin{equation}
    E_{0_R} = \left| \frac{v_2 - v_1}{v_2 + v_1} \right| E_{0_I}
    ,\quad
    E_{0_T} = \frac{2v_2}{v_2 + v_1} E_{0_I}
\end{equation}
or in terms of indices of refraction,
\begin{equation}
    E_{0_R} = \left| \frac{n_1 - n_2}{n_1 + n_2} \right| E_{0_I}
    ,\quad
    E_{0_T} = \frac{2n_2}{n_2 + n_1} E_{0_I}
\end{equation}

\subsubsection{Reflection and transmission coefficients}
\textbf Q. What fraction of the incident energy is reflected, and what fraction is transmitted?

The intensity (power per unit area) is
\[
    I = \frac{1}{2} \epsilon v E_0^2
\]
If $\mu_1 = \mu_2 = \mu_0$, then the \textbf{reflection coefficient} is
\begin{equation}
    R \equiv \frac{I_R}{I_I}
    = \left( \frac{E_{0_R}}{E_{0_I}} \right)^2
    = \left( \frac{n_1 - n_2}{n_1 + n_2} \right)^2
\end{equation}
and the \textbf{transmission coefficient} is
\begin{equation}
    T \equiv \frac{I_T}{I_I}
    = \frac{\epsilon_2 v_2}{\epsilon_1 v_1} \left( \frac{E_{0_T}}{E_{0_I}} \right)^2
    = \frac{4 n_1 n_2}{(n_1 + n_2)^2}
\end{equation}
Conservation of energy demands
\begin{equation}
    R + T = 1
\end{equation}

For instance, when light passes from air ($n_1 = 1$) into glass ($n_2 = 1.5$), $R = 0.04$ and $T = 0.96$; therefore, most of the light is transmitted through the interface.

\framed{%
    Homework problems: 9.15
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Reflection and Transmission at Oblique Incidence}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch09-reflection_transmission-02}
    \caption{Geometry of oblique incidence of light wave.}%
    \label{fig:ch09:ObliqueIncidence:1}
\end{figure}
We now turn to the more general case of oblique incidence, in which the incoming wave meets the boundary at an arbitrary angle $\theta_I$.

Suppose that a monochromatic plane wave
\begin{equation}
    \cxvec E_I(\mathbf r, t)
    =
    \cxvec E_{0_I} e^{i(\mathbf k_I \cdot \mathbf r - \omega t)}
    ,\quad
    \cxvec B_I(\mathbf r, t)
    =
    \frac{1}{v_1} \left( \uvec k_I \times \cxvec E_I \right)
\end{equation}
approaches from the left, giving rise to a reflected wave,
\begin{equation}
    \cxvec E_R(\mathbf r, t)
    =
    \cxvec E_{0_R} e^{i(\mathbf k_R \cdot \mathbf r - \omega t)}
    ,\quad
    \cxvec B_R(\mathbf r, t)
    =
    \frac{1}{v_1} \left( \uvec k_R \times \cxvec E_R \right)
\end{equation}
and a transmitted wave
\begin{equation}
    \cxvec E_T(\mathbf r, t)
    =
    \cxvec E_{0_T} e^{i(\mathbf k_T \cdot \mathbf r - \omega t)}
    ,\quad
    \cxvec B_T(\mathbf r, t)
    =
    \frac{1}{v_2} \left( \uvec k_T \times \cxvec E_T \right)
\end{equation}
All three waves have the same frequency $\omega$, and three wave numbers are related by $\omega = k v$; therefore,
\begin{equation}
    k_I v_1 = k_R v_1 = k_T v_2 = \omega
    ,\quad
    \text{or}\ k_I = k_R = \frac{v_2}{v_1} k_T = \frac{n_1}{n_2} k_T
    \label{eq:ch09:DispersionRelation}
\end{equation}

The combined fields in medium (1), $\cxvec E_I + \cxvec E_R$ and $\cxvec B_I + \cxvec B_R$, must now be joined to the fields, $\cxvec E_T$ and $\cxvec B_T$, in medium (2), honoring the boundary conditions at the interface ($z = 0$).
At the interface ($z = 0$), these all share the generic structure
\begin{equation}
    (\ ) e^{i(\mathbf k_I \cdot \mathbf r - \omega t)}
    +
    (\ ) e^{i(\mathbf k_R \cdot \mathbf r - \omega t)}
    =
    (\ ) e^{i(\mathbf k_T \cdot \mathbf r - \omega t)}
    \qquad (z = 0)
\end{equation}
or after canceling $e^{-i \omega t}$,
\begin{equation}
    (\ ) e^{i(\mathbf k_I \cdot \mathbf r)}
    +
    (\ ) e^{i(\mathbf k_R \cdot \mathbf r)}
    =
    (\ ) e^{i(\mathbf k_T \cdot \mathbf r)}
    \qquad (z = 0)
\end{equation}
The constant and exponent parts match separately.
So, from the equality of the exponents,
\begin{equation}
    \mathbf k_I \cdot \mathbf r
    =
    \mathbf k_R \cdot \mathbf r
    =
    \mathbf k_T \cdot \mathbf r
    \qquad (z = 0)
\end{equation}
Since this must hold for all points on the interface,
\begin{equation}
    \mathbf k_I^\|
    =
    \mathbf k_R^\|
    =
    \mathbf k_T^\|
    \qquad (z = 0)
    \label{eq:ch09:WaveNumberBC}
\end{equation}
We may as well orient our axes so that $\mathbf k_I = \mathbf k_I^\| + (k_I)_z \uvec z$ at the interface lies in the $xz$ plane (so, $\mathbf k_I = (k_I)_x\uvec x + (k_I)_z\uvec z$).
According to the above relation, so too $\mathbf k_R$ and $\mathbf k_T$ at $z = 0$.
Note that in our new coordinate system, $(k_I)_y = (k_R)_y = (k_T)_y = 0$ when $z = 0$.

Here comes the first of three fundamental laws of geometric optics:
\framed{%
    \textbf{First Law:}
    The incident, reflected, and transmitted wave vectors form a plane (called the \textbf{plane of incidence}), which also includes the normal to the interface (here the $z$ axis).
}

Meanwhile, writing Eq.~(\ref{eq:ch09:WaveNumberBC}) in terms of the angles shown in Figure~\ref{fig:ch09:ObliqueIncidence:1},
\begin{equation}
    k_I \sin\theta_I = k_R \sin\theta_R = k_T \sin\theta_T
    \qquad (z = 0)
\end{equation}
where $\theta_I$ is the \textbf{angle of incidence}, $\theta_R$ is the \textbf{angle of reflection}, and $\theta_T$ is the angle of transmission (or the \textbf{angle of refraction}), all of which are measured with respect to the normal.

Since $k_I = k_R$, it follows
\framed{%
    \textbf{Second Law:}
    The angle of incidence is equal to the angle of reflection,
    \begin{equation}
        \theta_I = \theta_R
    \end{equation}
    This is the \textbf{law of reflection}.
}

As for the transmitted angle, $k_I/k_T = v_2/v_1 = n_1/n_2$, so
\framed{%
    \textbf{Third Law:}
    \begin{equation}
        \frac{\sin\theta_T}{\sin\theta_I} = \frac{n_1}{n_2}
    \end{equation}
    This is the \textbf{law of refraction---Snell's law}.
}

With the choice of $\mathbf k_R$ and $\mathbf k_T$ at the interface, the exponential terms all cancel out at $z = 0$.
Then, the boundary conditions become
\begin{equation}
    \left.
        \begin{aligned}
            \text{(i)}
            &\quad
            \epsilon_1 \left( \cxvec E_{0_I} + \cxvec E_{0_R} \right)_z = \epsilon_2 \left( \cxvec E_{0_T} \right)_z
            \\
            \text{(ii)}
            &\quad
            \left( \cxvec B_{0_I} + \cxvec B_{0_R} \right)_z = \left( \cxvec B_{0_T} \right)_z
            \\
            \text{(iii)}
            &\quad
            \left( \cxvec E_{0_I} + \cxvec E_{0_R} \right)_{x,y} = \left( \cxvec E_{0_T} \right)_{x,y}
            \\
            \text{(iv)}
            &\quad
            \frac{1}{\mu_1} \left( \cxvec B_{0_I} + \cxvec B_{0_R} \right)_{x,y}
            =
            \frac{1}{\mu_2} \left( \cxvec B_{0_T} \right)_{x,y}
        \end{aligned}
    \right\}
\end{equation}
where $\cxvec B_0 = (1/v) \uvec k \times \cxvec E_0$ in each case.

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch09-reflection_transmission-03}
    \caption{Case where the polarization of the incident wave contained in the plane of incidence.}%
    \label{fig:ch09:ObliqueIncidence:2}
\end{figure}
Suppose the polarization of the incident wave is parallel to the plane of incidence (in this case, the $xz$ plane; Figure~\ref{fig:ch09:ObliqueIncidence:2}).
The reflected and transmitted waves are also polarized in this plane (Prob. 9.15).
\begin{itemize}
    \item Then, condition (i) reads
        \begin{equation}
            \epsilon_1 \left( -\cxvec E_{0_I} \sin\theta_I + \cxvec E_{0_R} \sin\theta_R \ \right)
            =
            \epsilon_2 \left( -\cxvec E_{0_T} \sin\theta_T \right)
            \label{eq:ch09:ObliqueIncidence:ConditionI}
        \end{equation}

        \item Condition (ii) adds nothing since the polarization of the magnetic field is out of plane (in the $y$ direction).

        \item Condition (iii) becomes
            \begin{equation}
                \cxvec E_{0_I} \cos\theta_I + \cxvec E_{0_R} \cos\theta_R
                =
                \cxvec E_{0_T} \cos\theta_T
            \label{eq:ch09:ObliqueIncidence:ConditionIII}
            \end{equation}

        \item Condition (iv) says
            \begin{equation}
                \frac{1}{\mu_1 v_1} \left( \cxvec E_{0_I} - \cxvec E_{0_R} \right)
                =
                \frac{1}{\mu_2 v_2} \cxvec E_{0_T}
            \label{eq:ch09:ObliqueIncidence:ConditionIV}
            \end{equation}
\end{itemize}
Given the laws of reflection and refraction, Eqs.~(\ref{eq:ch09:ObliqueIncidence:ConditionI}) and (\ref{eq:ch09:ObliqueIncidence:ConditionIV}) both reduce to
\begin{equation}
    \cxvec E_{0_I} - \cxvec E_{0_R} = \beta \cxvec E_{0_T}
\end{equation}
where
\begin{equation}
    \beta = \frac{\mu_1 v_1}{\mu_2 v_2} = \frac{\mu_1 n_2}{\mu_2 n_1}
\end{equation}
and Eq.~(\ref{eq:ch09:ObliqueIncidence:ConditionIII}) says
\begin{equation}
    \cxvec E_{0_I} + \cxvec E_{0_R} = \alpha \cxvec E_{0_T}
\end{equation}
where
\begin{equation}
    \alpha \equiv \frac{\cos\theta_T}{\cos\theta_I}
\end{equation}
Solving these two equations for the reflected and transmitted amplitudes, we obtain
\begin{equation}
    \boxed{%
        \tilde E_{0_R}
        =
        \left( \frac{\alpha - \beta}{\alpha + \beta} \right) \tilde E_{0_I}
        ,\quad
        \tilde E_{0_T}
        =
        \underbrace{\left( \frac{2}{\alpha + \beta} \right)}_{\color{blue} >\,0} \tilde E_{0_I}
    }
\end{equation}
These are known as \textbf{Fresnel's equations}, for the case of polarization in the plane of incidence.

\framed{%
    \begin{itemize}
        \item Since the factor is positive, the transmitted wave is always in phase with the incident wave.
        \item The reflected wave is either in phase if $\alpha > \beta$, or $180^\circ$ out of phase if $\alpha < \beta$.
\end{itemize}
}

Because $\alpha$ is a function of $\theta_I$,
\begin{equation}
    \alpha
    =
    \frac{\sqrt{1 - \sin^2\theta_T}}{\cos\theta_I}
    =
    \frac{\sqrt{1 - \left[ (n_1/n_2) \sin\theta_I \right]^2}}{\cos\theta_I}
    ,
\end{equation}
the amplitudes of the transmitted and reflected waves depends on $\theta_I$.
\framed{%
    \begin{itemize}
        \item In the case of \textit{normal incidence} ($\theta_I = 0$), $\alpha = 1$, and we recover Eq.~(\ref{eq:ch09:CXAmplitudeOfNormalIncidence}).
        \item At \textit{grazing incidence} ($\theta_I = 90^\circ$), $\alpha \rightarrow \infty$, and the wave is totally reflected.
\end{itemize}
}
There is an intermediate angle (called \textbf{Brewster's angle}), at which the reflected wave is completely extinguished.
This occurs when $\alpha = \beta$, or
\begin{equation}
    \sin^2\theta_B = \frac{1 - \beta^2}{(n_1/n_2)^2 - \beta^2}
\end{equation}
For the typical case of $\mu_1 \simeq \mu_2$, $\beta \simeq n_2/n_1$, so
\[
    \sin^2 \theta_B
    \simeq
    \frac{1 - \beta^2}{(1/\beta - \beta)(1/\beta + \beta)}
    =
    \frac{\beta^2}{1 + \beta^2}
\]
Making use of
\[
    \cos^2\theta_B
    \simeq
    1 - \frac{\beta^2}{1 + \beta^2}
    =
    \frac{1}{1 + \beta^2}
\]
it follows
\begin{equation}
    \tan\theta_B \simeq \beta = \frac{n_2}{n_1}
\end{equation}\\

The power per unit area striking the interface is $\mathbf S \cdot \uvec z$.
Thus the incident intensity is
\begin{equation}
    I_I = \frac{1}{2} \epsilon_1 v_1 E_{0_I}^2 \cos\theta_I
\end{equation}
while the reflected and transmitted intensities are
\begin{equation}
    I_R = \frac{1}{2} \epsilon_1 v_1 E_{0_R}^2 \cos\theta_R
    ,\quad
    I_T = \frac{1}{2} \epsilon_2 v_2 E_{0_T}^2 \cos\theta_T
\end{equation}

The reflection and transmission coefficients for waves polarized parallel to the plane of incidence are
\begin{equation}
    R \equiv \frac{I_R}{I_I}
    = \left( \frac{E_{0_R}}{E_{0_I}} \right)^2
    = \left( \frac{\alpha - \beta}{\alpha + \beta} \right)^2
\end{equation}
and
\begin{equation}
    T \equiv \frac{I_T}{I_I}
    = \frac{\epsilon_2 v_2}{\epsilon_1 v_1} \left( \frac{E_{0_T}}{E_{0_I}} \right)^2 \frac{\cos\theta_T}{\cos\theta_I}
    = \alpha \beta \left( \frac{2}{\alpha + \beta} \right)^2
\end{equation}
Note that $R + T = 1$ as required by conservation of energy.

\framed{%
    Homework problems: 9.18
}
