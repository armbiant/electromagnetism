\section{The Field of a Polarized Object}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Bound Charges}
\framed{%
    \textbf Q. Suppose that we have a polarized material with $\mathbf P$.
    What is the field produced by this object?\\

    \textbf A. We can chop the material up into infinitesimal dipoles and integrate to get the total!
}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-bound_charges-01}
    \caption{Polarized field calculation.}%
\end{figure}
The potential for a single dipole is
\begin{equation}
    V(\mathbf r)
    =
    \frac{1}{4\pi\epsilon_0} \frac{\mathbf p \cdot \uspvec}{\eta^2}
\end{equation}
where $\eta$ is the vector from the dipole to the point at which we are evaluating the potential.

In the present context, we substitute $\mathbf p = \mathbf P d\tau'$, and the total potential is given by
\begin{equation}
    V(\mathbf r)
    =
    \frac{1}{4\pi\epsilon_0} \int_{\mathcal V}
    \frac{\mathbf P(\mathbf r') \cdot \uspvec}{\eta^2} d\tau'
    \label{eq:ch04:PotentialOfPolarizedMaterial:1}
\end{equation}
Using the identity (the differentiation is with respect to the source point)
\[
    \nabla' \left( \frac{1}{\eta} \right) = \frac{\uspvec}{\eta^2}
\]
we can transform the potential as
\[
    V = \frac{1}{4\pi\epsilon_0} \int_{\mathcal V} \mathbf P \cdot \nabla' \left( \frac{1}{\eta} \right) d\tau'
\]
Integrating it by parts,
\[
    V = \frac{1}{4\pi\epsilon_0}
    \left[
        \int_{\mathcal V} \nabla' \cdot \left( \frac{\mathbf P}{\eta} \right) d\tau'
        -
        \int_{\mathcal V} \frac{1}{\eta} \left( \nabla' \cdot \mathbf P \right) d\tau'
    \right]
\]
Invoking the divergence theorem,
\begin{equation}
    V
    =
    \underbrace{\frac{1}{4\pi\epsilon_0} \oint_{\mathcal S} \frac{1}{\eta} \mathbf P \cdot d\mathbf a'}_{\color{blue}\text{(1)}}
    -
    \underbrace{\frac{1}{4\pi\epsilon_0} \int_{\mathcal V} \frac{1}{\eta} \left( \nabla' \cdot \mathbf P \right) d\tau'}_{\color{blue}\text{(2)}}
\end{equation}

Recall that the potential given by surface charge
\[
    V_{(1)} = \frac{1}{4\pi\epsilon_0} \oint \frac{\sigma_b}{\eta} da'
\]
Therefore, (1) is equivalent to the potential by a surface charge
\begin{equation}
    \boxed{%
        \sigma_b = \mathbf P \cdot \uvec n
    }
    \label{eq:ch04:SurfaceBoundCharge}
\end{equation}
where $\uvec n$ is the \textit{normal} unit vector outward from the surface of the material.

Likewise, (2) is equivalent to the potential by a volume charge $\rho_b$
\begin{equation}
    \boxed{%
        \rho_b = -\nabla \cdot \mathbf P
    }
    \label{eq:ch04:VolumeBoundCharge}
\end{equation}
within the material.
With these definitions, Eq.~(\ref{eq:ch04:PotentialOfPolarizedMaterial:1}) can be written in terms of $\rho_b$ and $\sigma_b$
\begin{equation}
    V(\mathbf r)
    =
    \frac{1}{4\pi\epsilon_0} \oint_{\mathcal S} \frac{\sigma_b}{\eta} da'
    +
    \frac{1}{4\pi\epsilon_0} \int_{\mathcal V} \frac{\rho_b}{\eta} d\tau'
    \label{eq:ch04:PotentialOfPolarizedMaterial:2}
\end{equation}

What this means is that the potential (and also the field) of a polarized object is the same as that produced by a volume charge density $\rho_b = -\nabla \cdot \mathbf P$ plus a surface charge density $\sigma_b = \mathbf P \cdot \uvec n$.
These are \textbf{bound charges}.

\framed{%
    \example{4.2}%
    Find the electric field produced by a uniformly polarized sphere of radius $R$.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-example_2-01}
    \caption{Example 4.2 Field due to a uniformly polarized object.}%
\end{figure}
We may choose the $z$ axis parallel to $\mathbf P$.
For the bound charges,
\begin{itemize}
    \item Since $\mathbf P$ is uniform, $\rho_b = -\nabla \cdot \mathbf P = 0$.
    \item Using $\uvec u = \uvec r$, the surface bound charge is
        \[
            \sigma_b = \mathbf P \cdot \uvec n = P \cos\theta
        \]
\end{itemize}

In Ex. 3.9, we computed the potential produced by a surface charge density of the form $k\cos\theta$:
\[
    V(r, \theta)
    =
    \left\{
        \begin{array}{ccl}
            \displaystyle \frac{P}{3\epsilon_0} r\cos\theta & & r \le R \\
            \\
            \displaystyle \frac{P}{3\epsilon_0} \frac{R^3}{r^2} \cos\theta & & r \ge R \\
        \end{array}
    \right.
\]

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-example_2-02}
    \caption{Example 4.2 Fields of a spherical object filled with uniformly polarized material.}%
\end{figure}
Since $z = r\cos\theta$, the field \textit{inside} is uniform:
\begin{equation}
    \mathbf E = -\nabla V = -\frac{P}{3\epsilon_0} \uvec z = -\frac{\mathbf P}{3\epsilon_0}
    ,\qquad r < R
\end{equation}
Outside the sphere, the potential is identical to that of a perfect dipole at the origin:
\begin{equation}
    V = \frac{1}{4\pi\epsilon_0} \frac{\mathbf p \cdot \uvec r}{r^2}
    ,\qquad r \ge R
\end{equation}
where $\mathbf p = (4/3) \pi R^3 \mathbf P$ is the total dipole moment of the sphere.

\framed{%
    Homework problems: 4.10, 4.11
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Physical Interpretation of Bound Charges}
\framed{%
    We introduced bound charges $\sigma_b$ and $\rho_b$ in the course of abstract manipulations on the integral Eq.\ (\ref{eq:ch04:PotentialOfPolarizedMaterial:1}).
    What is the physical meaning of these bound charges?
}

\subsubsection{Uniform Polarization}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-physical_interpretation_of_bound_charges-01}
    \caption{A series of dipoles.}%
\end{figure}
In fact, $\rho_b$ and $\sigma_b$ represent perfectly genuine accumulations of charge.
The basic idea is very simple: Suppose we have a long strip of dipoles.
Along the line, the head of one effectively cancels the tail of its neighbor, but at the end there are two charges left over.

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-physical_interpretation_of_bound_charges-02}
    \caption{Illustration of bound charge $\sigma_b$ calculation in a uniform field.}%
    \label{fig:ch04:IllustrationForBoundChargeCalculation}
\end{figure}
To calculate the actual amount of bound charge resulting from a given polarization, examine a tube of dielectric parallel to $\mathbf P$.
The dipole moment of the tiny chunk in Figure~\ref{fig:ch04:IllustrationForBoundChargeCalculation} is
\[
    P (A d)
\]
In terms of the charge $q$ at the end, the same dipole moment amounts to $qd$.
Therefore, the bound charge that piles up at the end of the tube (note the cancellation of intermediate charges) is
\[
    q = P A
\]
The surface charge density is then
\[
    \sigma_b = \frac{q}{A} = P
\]
For an oblique cut of area $A_{\rm end} = \frac{A}{\cos\theta}$,
\[
    \sigma_b = \frac{q}{A_{\rm end}} = \frac{q}{A} \cos\theta = P \cos\theta = \mathbf P \cdot \uvec n
\]

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-physical_interpretation_of_bound_charges-03}
    \caption{Illustration of bound charge $\rho_b$ calculation in a nonuniform field.}%
\end{figure}
\subsubsection{Nonuniform Polarization}
If the polarization is nonuniform, we get accumulations of bound charge within the material.
The net bound charge in a given volume is equal and opposite ($\because$ material is neutral) to the amount pushed out through the surface (i.e., $\sigma_b$):
\[
    \int_{\mathcal V} \rho_b d\tau
    =
    -\oint_{\mathcal S} \sigma_b da
    =
    -\oint_{\mathcal S} \mathbf P \cdot d\mathbf a
    =
    -\int_{\mathcal V} \nabla \cdot \mathbf P d\tau
\]
where we used divergence theorem.
Since this is true for any volume, we have
\[
    \rho_b = -\nabla \cdot \mathbf P
\]

\framed{%
    Homework problems: 4.13, 4.14
}
