\section{Waves in One Dimensions}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Wave Equation}
\framed{%
\textbf Q. What is a ``wave''?

\textbf A. A wave is a disturbance of a continuous medium that propagates with a {\color{blue}fixed shape} at {\color{blue}constant velocity}.
}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch09-traveling_disturbance-01}
    \caption{Traveling disturbance.}%
    \label{fig:ch09:TravelingDisturbance}%
\end{figure}
Let's start with the simple case: Fixed shape, constant velocity.
\\
From Figure~\ref{fig:ch09:TravelingDisturbance}, the disturbance traveling at $v$ keeps its shape which is given by the function $g(\xi)$.
Therefore,
\begin{equation}
    f(z, t) = g(z - vt) = f(z - vt, 0)
\end{equation}
The last equality is because $f(z, 0) = g(z)$.

It tells us that the function $f(z, t)$ depends on $z$ (i.e., space) and $t$ (i.e., time) only in the special combination $z - vt$.
This means that $g(\xi)$ can be any (differentiable) function.

Solutions of the \textbf{wave equation}
\begin{equation}
    \boxed{%
        \frac{\partial^2 f}{\partial z^2} = \frac{1}{v^2} \frac{\partial^2 f}{\partial t^2}
    }
    \label{eq:ch09:WaveEquation}
\end{equation}
have this property.
That is, any function of the form, $g(z - vt)$ (right traveling wave) or $h(z + vt)$ (left traveling wave), can be the solution of Eq.~(\ref{eq:ch09:WaveEquation}).
The most general solution is the linear combination of these
\begin{equation}
    f(z, t) = g(z - vt) + h(z + vt)
\end{equation}

\framed{%
    Homework problems: 9.2
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Sinusoidal Waves}
\subsubsection{(i) Terminology}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch09-sinusoidal_wave-01}
    \caption{Sinusoidal wave.}%
    \label{fig:ch09:SinusoidalWave}%
\end{figure}
Of all possible functional forms, the sinusoidal one
\begin{equation}
    f(z, t) = A \cos \left( k(z - vt) + \delta \right)
    \label{eq:ch09:SinusoidalWave:InTermsOfVelocity}
\end{equation}
is the most familiar (and useful).
Figure~\ref{fig:ch09:SinusoidalWave} illustrates what the parameters mean:
\begin{itemize}
    \item $A$: Amplitude of the wave
    \item Phase: The argument of the cosine, $k(z - vt) + \delta$
    \item $\delta$: Phase constant
    \item $k$: Wave number
    \item $\lambda = 2\pi/k$: Wavelength ($f(z + \lambda, t) = f(z, t)$)
    \item $T = 2\pi/(kv)$: Period ($f(z, t + T) = f(z, t)$)
    \item $\nu = 1/T = v/\lambda$: Frequency
    \item $\omega = 2\pi \nu = kv$: Angular frequency
\end{itemize}
It is nicer to write Eq.~(\ref{eq:ch09:SinusoidalWave:InTermsOfVelocity}) in terms of $\omega$ and $k$:
\begin{equation}
    \left\{
        \begin{array}{lcl}
            f(z, t) = A \cos(\phantom{-}kz - \omega t + \delta) & & \text{right propagation} \\
            \\
            f(z, t) = A \cos(-kz - \omega t + \delta) & & \text{left propagation} \\
        \end{array}
    \right.
    \label{eq:ch09:SinusoidalWave}
\end{equation}
Note that the equation for the left-traveling wave amounts to flipping the sign of $k$.

\subsubsection{(ii) Complex notation}
It is most convenient to work with sinusoidal waves in complex domain.
Using Euler's formula, the first of Eq.~(\ref{eq:ch09:SinusoidalWave}) can be written
\begin{equation}
    f(z, t) = \Re \left[ A e^{i(kz - \omega t + \delta)} \right]
\end{equation}
where $\Re(\xi)$ denotes the real part of the complex number $\xi$.

By introducing the complex wave function
\begin{equation}
    \tilde f(z, t) = \underbrace{A e^{i\delta}}_{\color{blue}=\tilde A} e^{i(kz - \omega t)}
    = \tilde A e^{i(kz - \omega t)}
\end{equation}
where $\tilde A = A e^{i\delta}$ is the complex amplitude (i.e., amplitude + phase constant), it follows
\begin{equation}
    f(z, t) = \Re \left( \tilde f(z, t) \right)
\end{equation}

The main advantage of the complex notation is that exponentials are much easier to manipulate than sines and cosines.
But, keep in mind that the actual physical quantities are real-valued.
You can manipulate the complex counterparts as long as the operations are linear.
Otherwise, you need to exercise caution.
For example, a na\"ive calculation of the length of a real vector and its complex counterpart will give you a different result:
\begin{equation}
    \mathbf E \cdot \mathbf E \ne \Re \left( \cxvec E \cdot \cxvec E \right)
\end{equation}

\separator%

\example{9.1}%
Suppose you want to combine two sinusoidal waves:
\[
    f_3 = f_1 + f_2
    = \Re\left( \tilde f_1 \right) + \Re\left( \tilde f_2 \right)
    = \Re\left( \tilde f_1 + \tilde f_2 \right)
    = \Re\left( \tilde f_3 \right)
\]
with $\tilde f_3 = \tilde f_1 + \tilde f_2$.
This is a linear operation and you can simply add the corresponding complex wave functions.

If they have the same wave frequency and wave number,
\[
    \tilde f_3
    =
    \tilde A_1 e^{i(kz - \omega t)}
    +
    \tilde A_2 e^{i(kz - \omega t)}
    =
    \tilde A_3 e^{i(kz - \omega t)}
\]
where
\[
    \tilde A_3 = \tilde A_1 + \tilde A_2
    ,\quad \text{or} \quad
    A_3 e^{i\delta_3}
    =
    A_1 e^{i\delta_1}
    +
    A_2 e^{i\delta_2}
\]
Therefore, the combined wave reads
\[
    f_3(z, t) = \Re\left( \tilde f_3(z, t) \right)
    = A_3 \cos(kz - \omega t + \delta_3)
\]
where you can figure out $A_3$ and $\delta_3$ easily.

\separator%

\subsubsection{(iii) Linear combinations of sinusoidal waves}
Any wave can be expressed as a linear combination of sinusoidal ones:
\begin{equation}
    \tilde f(z, t)
    =
    \int_{-\infty}^\infty \tilde A(k) e^{i(kz - \omega t)} dk
\end{equation}
where the amplitude function in terms of initial conditions, $f(z, 0)$ and $\dot{f}(z, 0)$, is given by
\begin{equation}
    \tilde A(k)
    =
    \frac{1}{2\pi} \int_{-\infty}^\infty
    \tilde f(z, 0) e^{-ikz} dz
\end{equation}
The angular frequency $\omega$ is understood to be a function of $k$.

\framed{%
    Homework problems: 9.3
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Boundary Conditions: Reflection and Transmission}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch09-two_types_of_strings_joined-01}
    \caption{Two strings of different material joint at $z = 0$.}%
\end{figure}
Suppose that two different (e.g., different mass, tension, etc.) types of string are joint at $z = 0$.

The incident wave (given as a complex function)
\begin{equation}
    \tilde f_I(z, t) = \tilde A_I e^{i(k_1 z - \omega t)}
    \qquad (z < 0)
\end{equation}
coming in from the left, gives rise to a reflected wave
\begin{equation}
    \tilde f_R(z, t) = \tilde A_R e^{i(-k_1 z - \omega t)}
    \qquad (z < 0)
\end{equation}
traveling back along string 1, in addition to a transmitted wave
\begin{equation}
    \tilde f_T(z, t) = \tilde A_T e^{i(k_2 z - \omega t)}
    \qquad (z > 0)
\end{equation}
which continues on to the right in string 2.
Obviously, the reflected wave has the same wave number as the incident one.

\textit{All parts of the system are oscillating at the same frequency $\omega$.}
Since the wave velocities are different in the two strings, however, the wavelengths and thus the wave number are also different:
\begin{equation}
    \frac{\lambda_1}{\lambda_2} = \frac{k_2}{k_1} = \frac{v_1}{v_2}
\end{equation}

The net disturbance of the string is
\begin{equation}
    \tilde f(z, t)
    =
    \left\{
        \begin{array}{lcl}
            \tilde A_I e^{i(k_1 z - \omega t)}
            +
            \tilde A_R e^{i(-k_1 z - \omega t)}
            & &
            z < 0
            \\\\
            \tilde A_T e^{i(k_2 z - \omega t)}
            & &
            z > 0
        \end{array}
    \right.%
    \label{eq:ch09:NetComplexDisturbance}
\end{equation}
The function (both real and complex) must be continuous at the join:
\begin{equation}
    \tilde f(0^-, t) = \tilde f(0^+, t)
\end{equation}
And so must its derivative (as long as the knot is itself of negligible mass)
\begin{equation}
    \left.\frac{\partial \tilde f}{\partial z}\right|_{0^-}
    =
    \left.\frac{\partial \tilde f}{\partial z}\right|_{0^+}
\end{equation}
When applied to Eq.~(\ref{eq:ch09:NetComplexDisturbance}), we get
\[
    \tilde A_I + \tilde A_R = \tilde A_T
    ,\quad
    k_1(\tilde A_I - \tilde A_R) = k_2 \tilde A_T
\]
Solving for $\tilde A_R$ and $\tilde A_T$,
\begin{equation}
    \tilde A_R = \left( \frac{k_1 - k_2}{k_1 + k_2} \right) \tilde A_I
    ,\quad
    \tilde A_T = \left( \frac{2k_1}{k_1 + k_2} \right) \tilde A_I
\end{equation}
or in terms of velocity ($v = \omega/k$),
\begin{equation}
    \tilde A_R = \left( \frac{v_2 - v_1}{v_2 + v_1} \right) \tilde A_I
    ,\quad
    \tilde A_T = \left( \frac{2v_2}{v_2 + v_1} \right) \tilde A_I
    \label{eq:ch09:ReflectTransmitAmplitudes}
\end{equation}
The real amplitudes and phases are related by
\begin{equation}
    A_R e^{i\delta_R} = \left( \frac{v_2 - v_1}{v_2 + v_1} \right) A_I e^{i\delta_I}
    ,\quad
    A_T e^{i\delta_T} = \left( \frac{2v_2}{v_2 + v_1} \right) A_I e^{i\delta_I}
\end{equation}

If $v_2 > v_1$ (e.g., lighter second string), then
\begin{equation}
    \left\{
        \begin{array}{lcl}
            \displaystyle A_R = \frac{v_2 - v_1}{v_2 + v_1} A_I,
            & &
            \delta_R = \delta_I
            \\\\
            \displaystyle A_T = \frac{2v_2}{v_2 + v_1} A_I,
            & &
            \delta_T = \delta_I
        \end{array}
    \right.
\end{equation}
Both the reflected and transmitted waves have the same phase (\textit{in phase}) as the incident one ($\delta_R = \delta_T = \delta_I$).

If $v_2 < v_1$ (e.g., heavier second string), the amplitude and phase of the transmitted wave stays the same.
However, the complex amplitude of the reflected wave reads
\[
    A_R e^{i\delta_R}
    =
    -\underbrace{\left( \frac{v_1 - v_2}{v_1 + v_2} \right)}_{\color{blue}>\,0} A_I e^{i\delta_I}
    =
    \left( \frac{v_1 - v_2}{v_1 + v_2} \right) A_I e^{i(\delta_I + \pi)}
\]
Therefore, the reflected wave is \textit{out of phase} by $180^\circ$:
\[
    \delta_R = \delta_I + \pi
\]

If $v_2 = 0$ (e.g., massive second string), then
\[
    A_R = A_I
    ,\quad
    A_T = 0
\]

\framed{%
    Homework problems: 9.5
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Polarization}
\begin{itemize}
    \item \textbf{Longitudinal} wave: The displacement is parallel to the direction of propagation.
    \item \textbf{Transverse} wave: The displacement is perpendicular to the direction of propagation.
\end{itemize}

\begin{figure}[ht]
    \centering
    \includegraphics[width=25pc]{assets/ch09-wave_polarizations-01}
    \caption{Wave polarization.}%
    \label{fig:ch09:WavePolarization}%
\end{figure}
Transverse waves occur in two independent states of \textbf{polarization}:
\begin{itemize}
    \item ``Vertical'' polarization (Figure~\ref{fig:ch09:WavePolarization}a) given by
        \begin{equation}
            \cxvec f_v(z, t) = \tilde A e^{i(kz - \omega t)} \uvec x
        \end{equation}

    \item ``Horizontal'' polarization (Figure~\ref{fig:ch09:WavePolarization}b) given by
        \begin{equation}
            \cxvec f_h(z, t) = \tilde A e^{i(kz - \omega t)} \uvec y
        \end{equation}
\end{itemize}

In general, the polarization direction can be along any other direction in the $xy$ plane (Figure~\ref{fig:ch09:WavePolarization}c) is
\begin{equation}
    \cxvec f(z, t) = \tilde A e^{i(kz - \omega t)} \uvec n
\end{equation}
where the \textbf{polarization vector} $\uvec n$ defines the \textit{plane of vibration}.

Because the waves are transverse, $\uvec n$ is perpendicular to the direction of propagation:
\begin{equation}
    \uvec n \cdot \uvec z = 0
\end{equation}

In terms of \textbf{polarization angle} $\theta$ (Figure~\ref{fig:ch09:WavePolarization}c),
\begin{equation}
    \uvec n = \cos\theta \uvec x + \sin\theta \uvec y
\end{equation}
and thus
\begin{equation}
    \cxvec f(z, t)
    =
    \underbrace{\left( \tilde A \cos\theta \right) e^{i(kz - \omega t)} \uvec x}_{\color{blue}\text{``vertical'' polarization}}
    +
    \underbrace{\left( \tilde A \sin\theta \right) e^{i(kz - \omega t)} \uvec y}_{\color{blue}\text{``horizontal'' polarization}}
\end{equation}
Therefore, the wave of arbitrary polarization can be considered a superposition of two (typically orthogonal) waves.

\framed{%
    Homework problems: 9.8a
}
