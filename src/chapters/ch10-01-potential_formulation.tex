\section{The Potential Formulation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Scalar and Vector Potentials}
In this section, the \textbf{goal} is to find the general solution to Maxwell's equations,
\begin{equation}
    \left.
        \begin{array}{rlcrl}
            \text{(i)}   & \displaystyle \nabla \cdot \mathbf E = \frac{\rho}{\epsilon_0}, &&
            \text{(iii)} & \displaystyle \nabla \times \mathbf E = -\frac{\partial\mathbf B}{\partial t}
            \\\\
            \text{(ii)}  & \displaystyle \nabla \cdot \mathbf B = 0, &&
            \text{(iv)}  & \displaystyle \nabla \times \mathbf B = \mu_0 \mathbf J + \mu_0\epsilon_0 \frac{\partial\mathbf E}{\partial t}
        \end{array}
    \right\}
\end{equation}

\framed{%
    \textbf Q. Given $\rho(\mathbf r, t)$ and $\mathbf J(\mathbf r, t)$, what are the fields $\mathbf E(\mathbf r, t)$ and $\mathbf B(\mathbf r, t)$ that satisfy (i)--\@(iv)?
}

Of course, in the static case ($\frac{\partial\rho}{\partial t} = 0$ and $\frac{\partial\mathbf J}{\partial t} = 0$), Coulomb's law and the Biot-Savart law provide the answer.
We will generalize these laws to \textbf{time-dependent configurations}.\\

It is easier to work with the corresponding potentials rather than the fields.
We can define the vector potential $\mathbf A$ in the same way as we did in magnetostatics.
However, we can no longer write the electric field as the gradient of a scalar potential $V$ in the time-dependent case.
\begin{center}
    \begin{tabular}[ht]{ccc}
        \hline
        Static Case
        & &
        Dynamic Case
        \\
        \hline
        \\
        $\mathbf B = \nabla \times \mathbf A$
        & $\rightarrow$ &
        $\mathbf B = \nabla \times \mathbf A$
        \\\\
        $\mathbf E = -\nabla V$ ($\because\,\nabla \times \mathbf E = 0$)
        & $\rightarrow$ &
        $\displaystyle \nabla \times \mathbf E = -\frac{\partial\mathbf B}{\partial t} = \nabla \times \left( -\frac{\partial\mathbf A}{\partial t} \right)$
        \\\\
        \hline
    \end{tabular}
\end{center}
Instead, since
\[
    \nabla \times \left( \mathbf E + \frac{\partial\mathbf A}{\partial t} \right) = 0
\]
we can write
\[
    \mathbf E + \frac{\partial\mathbf A}{\partial t} = -\nabla V
\]
Therefore, the electric field in the \textit{time-dependent} regime becomes
\begin{equation}
    \mathbf E = -\nabla V - \frac{\partial\mathbf A}{\partial t}
\end{equation}
Putting them into Maxwell's equations, we find that (ii) and (iii) are trivially satisfied:
\[
    \begin{aligned}
        \nabla \times \mathbf E
        &=
        -\cancel{\nabla \times \nabla V} - \nabla \times \frac{\partial\mathbf A}{\partial t}
        \\
        =
        -\frac{\partial\mathbf B}{\partial t}
        &=
        -\frac{\partial}{\partial t} (\nabla \times \mathbf A)
        =
        -\nabla \times \frac{\partial\mathbf A}{\partial t}
    \end{aligned}
\]
Gauss's law (i) becomes
\[
    \frac{\rho}{\epsilon_0}
    =
    \nabla \cdot \mathbf E
    =
    -\nabla^2 V - \nabla \cdot \frac{\partial\mathbf A}{\partial t}
\]
Therefore,
\begin{equation}
    \boxed{%
        \nabla^2 V + \frac{\partial}{\partial t}(\nabla \cdot \mathbf A) = -\frac{\rho}{\epsilon_0}
    }
    \label{eq:ch10:PotentialDifferential1}
\end{equation}
Amp\`ere-Maxwell law (iv) becomes
\[
    \left.
        \begin{aligned}
            \text{LHS:}
        &\quad
        \nabla \times \mathbf B
        =
        \nabla \times (\nabla \times \mathbf A)
        =
        \nabla (\nabla \cdot \mathbf A) - \nabla^2 \mathbf A
        \\
        \\
        \text{RHS:}
        &\quad
        \begin{aligned}
            \mu_0 \mathbf J + \mu_0 \epsilon_0 \frac{\partial\mathbf E}{\partial t}
            &=
            \mu_0 \mathbf J + \mu_0 \epsilon_0 \frac{\partial}{\partial t} \left( -\nabla V - \frac{\partial\mathbf A}{\partial t} \right)
            \\
            &=
            \mu_0 \mathbf J - \mu_0 \epsilon_0 \nabla \left( \frac{\partial V}{\partial t} \right) - \mu_0 \epsilon_0 \frac{\partial^2\mathbf A}{\partial t^2}
        \end{aligned}
        \end{aligned}
    \right\}
\]
Equating them, we get
\begin{equation}
    \boxed{%
        \left( \nabla^2\mathbf A - \mu_0 \epsilon_0 \frac{\partial\mathbf A}{\partial t} \right)
        -
        \nabla \left( \nabla \cdot \mathbf A + \mu_0 \epsilon_0 \frac{\partial V}{\partial t} \right)
        =
        -\mu_0 \mathbf J
    }
    \label{eq:ch10:PotentialDifferential2}
\end{equation}

\framed{%
    \example{10.1}%
    Find the charge and current distributions that would give rise to the potentials
    \[
        V = 0
        ,\quad
        \mathbf A =
        \left\{
            \begin{array}{lcc}
                \displaystyle \frac{\mu_0 k}{4c} (ct - |x|)^2 \uvec z,
                & & \text{for}\ |x| < ct
                \\
                \displaystyle \mathbf 0,
                & & \text{for}\ |x| > ct
            \end{array}
        \right.
    \]
    where $k$ is a constant, and $c = 1/\sqrt{\epsilon_0 \mu_0}$.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics[width=25pc]{assets/ch10-example_1-01}
    \caption{Example 10.1 Fields for the given potentials.}%
\end{figure}
\textbf{Step 1.} Get the fields.
\\
Electric field:
\[
    \mathbf E = -\nabla V -\frac{\partial\mathbf A}{\partial t}
    =
   \left\{
       \begin{array}{lcc}
           \displaystyle -\frac{\mu_0 k}{2} (ct - |x|) \uvec z,
           & & \text{for}\ |x| < ct
           \\
           \displaystyle \mathbf 0,
           & & \text{for}\ |x| > ct
       \end{array}
   \right.
\]
Magnetic field:
\[
    \mathbf B = \nabla \times \mathbf A
    = -\frac{\partial A_z}{\partial x} \uvec y
    =
    \left\{
        \begin{array}{lcc}
            \displaystyle
            \pm \frac{\mu_0 k}{2c} (ct - |x|) \uvec y,
            &
            \left(
                \begin{matrix}
                    +: & x > 0 \\
                    -: & x < 0 \\
                \end{matrix}
            \right)
            & \text{for}\ |x| < ct
            \\
            \displaystyle \mathbf 0,
            & & \text{for}\ |x| > ct
        \end{array}
    \right.
\]

\textbf{Step 2.} Use Maxwell's equations.
\\
From Gauss's law,
\[
    \rho = \epsilon_0 \nabla \cdot \mathbf E = 0
\]
so $\rho = 0$ everywhere.
\\
Meanwhile,
\[
    \begin{aligned}
        \nabla \times \mathbf B
        &=
        \uvec z \frac{\partial B_y}{\partial x}
        =
        \uvec z \left( -\frac{\mu_0 k}{2c} \right)
        \\
        \mu_0 \epsilon_0 \frac{\partial\mathbf E}{\partial t}
        &=
        \mu_0 \epsilon_0 \left( -\frac{\mu_0 c k}{2} \right) \uvec z
        =
        \uvec z \left( -\frac{\mu_0 k}{2c} \right)
    \end{aligned}
\]
for $|x| < ct$ and $x \ne 0$.
(Note that $\partial B_y/\partial x$ diverges at $x = 0$.)
Putting them into the Amp\`ere-Maxwell law, we find that $\mathbf J = 0$ everywhere except when $x = 0$.
\\

At $x = 0$, $B_y$ is discontinuous (due to the surface current).
The surface current is given by the boundary condition
\[
    \mathbf B_{\rm above} - \mathbf B_{\rm below}
    =
    \mu_0 \mathbf K \times \uvec n
\]
Since $\mathbf B = B_y \uvec y$ and $\uvec n = \uvec x$, it follows that $\mathbf K = K \uvec z$, and
\[
    \left.
        \begin{aligned}
            B_{y,\rm above} &= \phantom{-}\frac{\mu_0 k t}{2} \\
            B_{y,\rm below} &= -\frac{\mu_0 k t}{2} \\
        \end{aligned}
    \right\}
    \quad \rightarrow \quad
    \therefore\,
    \mathbf K = k t \uvec z
\]
\\

Evidently, we have a uniform surface current flowing in the $z$ direction over the plane $x = 0$, which starts up at $t = 0$, and increases linearly with $t$.
\\

Information (or ``news'') of the increase of the current travels out at the speed of light, $c$.
Therefore, for the points $|x| > ct$, the ``message'' has not arrived yet, hence zero fields.
For the points $|x| < ct$, the fields are from the current at time $t' = t - \frac{|x|}{c}$ (not at time $t$), i.e., $\mathbf K (t - \frac{|x|}{c})$.

\framed{%
    Homework problems: 10.1, 10.2
}

\framed{%
    \problem{10.1}%
    Show that the differential equations for $V$ and $\mathbf A$ (Eqs.~\ref{eq:ch10:PotentialDifferential1} and~\ref{eq:ch10:PotentialDifferential2}) can be written in the more symmetric form
    \begin{equation}
        \left.
            \begin{aligned}
                \Box^2 V + \frac{\partial L}{\partial t}
                &=
                -\frac{1}{\epsilon_0} \rho
                \\
                \Box^2 \mathbf A - \nabla L
                &=
                -\mu_0 \mathbf J
            \end{aligned}
        \right\}
    \end{equation}
    where
    \[
        \Box^2 \equiv \nabla^2 - \mu_0 \epsilon_0 \frac{\partial^2}{\partial t^2}
        \quad\text{and}\quad
        L \equiv \nabla \cdot \mathbf A + \mu_0 \epsilon_0 \frac{\partial V}{\partial t}
    \]
}
\solution%

From Eq. (\ref{eq:ch10:PotentialDifferential1}),
\[
    \begin{aligned}
    \nabla^2 V + \frac{\partial}{\partial t}(\nabla \cdot \mathbf A)
    &=
    \nabla^2 V
    {\color{blue}\,-\,\mu_0 \epsilon_0 \frac{\partial^2 V}{\partial t^2}}
    +
    \frac{\partial}{\partial t}(\nabla \cdot \mathbf A)
    {\color{blue}\,+\,\mu_0 \epsilon_0 \frac{\partial^2 V}{\partial t^2}}
    \\
    &=
    \underbrace{\left( \nabla^2 - \mu_0 \epsilon_0 \frac{\partial^2}{\partial t^2} \right)}_{\color{blue} \Box^2} V
    +
    \frac{\partial}{\partial t} \underbrace{\left( \nabla \cdot \mathbf A + \mu_0 \epsilon_0 \frac{\partial V}{\partial t} \right)}_{\color{blue} L}
    \\
    &=
    -\frac{\rho}{\epsilon_0}
    \end{aligned}
\]

From Eq. (\ref{eq:ch10:PotentialDifferential2}), it is easy to obtain the second relation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Gauge Transformations}
\framed{%
    \textbf{Gauge freedom}: In the potential representation, we are free to impose extra conditions on $V$ and $\mathbf A$, as long as nothing happens to $\mathbf E$ and $\mathbf B$.
}

Consider two sets of potentials, $(V, \mathbf A)$ and $(V', \mathbf A')$, that correspond to the same $\mathbf E$ and $\mathbf B$.
They are related to each other by
\[
    \mathbf A' = \mathbf A + \boldsymbol\alpha
    \quad\text{and}\quad
    V' = V + \beta
\]
What must $\boldsymbol\alpha$ and $\beta$ be?
Both vector potentials must result in the same magnetic field
\[
    \nabla \times \mathbf A = \mathbf B = \nabla \times \mathbf A'
    \quad\rightarrow\quad
    \nabla \times \boldsymbol\alpha = 0
\]
Therefore, $\boldsymbol\alpha$ must curl-less, which means it can be written as the gradient of a scalar function $\lambda$
\[
    \boldsymbol \alpha = \nabla \lambda
\]
For the electric field
\[
    \nabla V + \frac{\partial\mathbf A}{\partial t}
    =
    -\mathbf E
    =
    \nabla V' + \frac{\partial\mathbf A'}{\partial t}
    \quad\rightarrow\quad
    \nabla \beta + \frac{\partial\mathbf{\boldsymbol\alpha}}{\partial t} = 0
\]
Substituting $\boldsymbol \alpha$, we find
\[
    \nabla \left( \beta + \frac{\partial \lambda}{\partial t} \right) = 0
    \quad\rightarrow\quad
    \beta = -\frac{\partial\lambda}{\partial t}
\]
{\color{gray}(Technically, $\beta + \frac{\partial\lambda}{\partial t} = k(t)$. But, we can always redefine $\lambda$ such that $\partial\lambda/\partial t$ cancels $k(t)$.)}
\\

In conclusion,
\begin{equation}
    \left.
        \begin{aligned}
            \mathbf A' &= \mathbf A + \nabla \lambda \\
            V' &= V - \frac{\partial\lambda}{\partial t}
        \end{aligned}
    \right\}
\end{equation}
For any scalar function $\lambda(\mathbf r, t)$, we can add $\nabla \lambda$ to $\mathbf A$ and simultaneously subtract $\partial\lambda/\partial t$ from $V$, without affecting both $\mathbf E$ and $\mathbf B$.
Particularly, we can use the gauge transformations to adjust the divergence of $\mathbf A$.

\framed{%
    Homework problems: 10.3, 10.4
}

\framed{%
    \problem{10.3}%
    \begin{enumerate}
        \item [(a)] Find the fields, and the charge and current distributions, corresponding to
            \[
                V(\mathbf r, t) = 0
                ,\quad
                \mathbf A(\mathbf r, t)
                =
                -\frac{1}{4\pi\epsilon_0} \frac{q t}{r^2} \uvec r
            \]

        \item [(b)] Use the gauge function
            \[
                \lambda = -\frac{1}{4\pi\epsilon_0} \frac{qt}{t}
            \]
            to transform the potentials, and comment on the result.
    \end{enumerate}
}
\solution%
\begin{enumerate}
    \item [(a)]
        \[
            \left\{
                \begin{aligned}
                    \mathbf B &= \nabla \times \mathbf A = 0 \\
                    \mathbf E &= -\nabla V - \frac{\partial\mathbf A}{\partial t} = \frac{1}{4\pi\epsilon_0} \frac{q}{r^2} \uvec r
                \end{aligned}
            \right.
        \]
        \[
            \left\{
                \begin{aligned}
                    \rho &= \epsilon_0 (\nabla \cdot \mathbf E) = q \delta^3(\mathbf r) \\
                    \mathbf J &= \frac{1}{\mu_0} (\nabla \times \mathbf B) - \epsilon_0\frac{\partial\mathbf E}{\partial t} = 0
                \end{aligned}
            \right.
        \]

    \item [(b)]
        \[
            \left\{
                \begin{aligned}
                    V' &= V - \frac{\partial\lambda}{\partial t} = \frac{1}{4\pi\epsilon_0} \frac{q}{r} \\
                    \mathbf A' &= \mathbf A + \nabla \lambda = 0
                \end{aligned}
            \right.
        \]
\end{enumerate}

It is clear from $V'$ and $\mathbf A'$ that the fields are created by a point charge at rest.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Coulomb Gauge and Lorenz Gauge}
\subsubsection{Coulomb Gauge}
As in magnetostatics, we pick
\begin{equation}
    \nabla \cdot \mathbf A = 0
\end{equation}

With this, Eq. (\ref{eq:ch10:PotentialDifferential1}) becomes Poisson's equation
\begin{equation}
    \nabla^2 V = -\frac{\rho}{\epsilon_0}
\end{equation}
whose solution is
\begin{equation}
    V(\mathbf r, t)
    =
    \frac{1}{4\pi\epsilon_0} \int
    \frac{\rho(\mathbf r', t)}{\eta} d\tau'
\end{equation}
with boundary condition $V = 0$ at infinity.

Here, the potential is determined by the charge distribution at present time $t$.
There is no causality problem as far as the electric field is concerned, because calculation of $\mathbf E$ also involves $\partial\mathbf A/\partial t$ which will compensate for this ``instantaneous'' effect.

Meanwhile, with the Coulomb gauge, Eq.~(\ref{eq:ch10:PotentialDifferential2}) reads
\begin{equation}
    \nabla^2\mathbf A - \mu_0 \epsilon_0 \frac{\partial\mathbf A}{\partial t}
    =
    -\mu_0 \mathbf J
    +\mu_0 \epsilon_0 \nabla \left( \frac{\partial V}{\partial t} \right)
\end{equation}
This is not easy to solve.
\\

The advantage of the Coulomb gauge is that the scalar potential is particularly simple to calculate.

\subsubsection{Lorenz Gauge}
Choose $\lambda$ so that
\begin{equation}
    \boxed{%
        \nabla \cdot \mathbf A = -\mu_0 \epsilon_0 \frac{\partial V}{\partial t}
    }
\end{equation}
Then, Eq. (\ref{eq:ch10:PotentialDifferential2}) becomes
\[
    \nabla^2 \mathbf A - \mu_0\epsilon_0\frac{\partial^2\mathbf A}{\partial t^2}
    = -\mu_0 \mathbf J
\]
and Eq. (\ref{eq:ch10:PotentialDifferential1})
\[
    \nabla^2 V - \mu_0\epsilon_0\frac{\partial^2 V}{\partial t^2}
    = -\frac{1}{\epsilon_0} \rho
\]

The virtue of the Lorenz gauge is that it treats $V$ and $\mathbf A$ on an equal footing:
With the d'Alembert operator (or \textbf{D'Alembertian})
\begin{equation}
    \Box^2 \equiv \nabla^2 - \mu_0\epsilon_0 \frac{\partial^2}{\partial t^2}
\end{equation}
the two equations read
\begin{equation}
    \boxed{%
        \begin{aligned}
            \text{(i)}
            &\quad
            \Box^2 V = -\frac{1}{\epsilon_0} \rho \\
            \text{(ii)}
            &\quad
            \Box^2 \mathbf A = -\mu_0 \mathbf J \\
        \end{aligned}
    }
    \label{eq:ch10:PoissonEquation:4D}
\end{equation}
In special relativity, this can be regarded as four-dimensional versions of Poisson's equation.
\\

In the Lorenz gauge, $V$ and $\mathbf A$ satisfy the \textbf{inhomogeneous wave equation}, with a source term on the right.
From now on, we will use the \textit{Lorenz gauge exclusively}, and the whole of electrodynamics reduces to the problem of solving the inhomogeneous wave equation for a specified source.

\framed{%
    Homework problems: 10.5, 10.6, 10.7
}
