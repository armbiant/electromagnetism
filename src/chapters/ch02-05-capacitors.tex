\setcounter{section}{4}
\section{Conductors}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setcounter{subsection}{3}
\subsection{Capacitors}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch02-two_conductors-01}
    \caption{Two conductors with charges $\pm Q$, respectively.}%
\end{figure}
Suppose that we have two conductors with charges, $+Q$ and $-Q$, respectively.
The potential difference between them is
\[
    V = V_+ - V_- = - \int_{(-)}^{(+)} \mathbf E \cdot d\mathbf l
\]
In general, we don't know how the charge distributes itself over the two conductors; and calculating the resulting field would be difficult if the shape is complicated.\\

But, we \textit{do} know that $E \propto Q$ and thus $V \propto Q$.
The constant of proportionality is called the \textbf{capacitance} of the arrangement:
\begin{equation}
    \boxed{%
        C \equiv \frac{Q}{V}
    }
\end{equation}

Capacitance is \textit{purely geometrical} quantity, only determined by the sizes, shapes, and separation of two conductors.

In SI units, $C$ is measured in \textbf{farads}: $[\mathrm F] = [\mathrm C]/[\mathrm V]$.
Typically, capacitance is measured between $10^{-12}\,\mathrm F$ and $10^{-6}\,\mathrm F$ in electronic parts.\\

$V$ is, by definition, the potential of the \textit{positive} conductor.
Likewise, $Q$ is the charge of \textit{positive} conductor.

\framed{%
    \example{2.11}
    Find the capacitance of a parallel-plate capacitor consisting of two metal surfaces of area $A$ held a distance $d$ apart.
}
\solution%
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch02-example_11-01}
    \caption{Example 2.11 Parallel-plate capacitor of area $A$.}%
\end{figure}

We can put $+Q$ on the top plate and $-Q$ on the bottom.
The charge will be uniformly spread with density $\pm Q/A$,
which yields the field
\[
    E = \frac{1}{\epsilon_0} \frac{Q}{A}
\]
The potential difference ($V = E d$) is then
\[
    V = \frac{Q}{A \epsilon_0} d
\]
and the capacitance
\[
    C = \frac{Q}{V} = \frac{A \epsilon_0}{d}
\]

\framed{%
    \example{2.12}
    Find the capacitance of two concentric spherical metal shells, with radii $a$ and $b$.
}
\solution%
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch02-example_12-01}
    \caption{Example 2.12 Spherical capacitor.}%
\end{figure}

The electric field in the region between the two spheres is
\[
    \mathbf E
    =
    \frac{1}{4\pi\epsilon_0} \frac{Q}{r^2} \uvec r
\]
The potential difference is
\[
    V
    =
    - \int_b^a \mathbf E \cdot d\mathbf l
    =
    -\frac{Q}{4\pi\epsilon_0}
    \int_b^a \frac{1}{r^2} dr
    =
    \frac{Q}{4\pi\epsilon_0} \left( \frac{1}{a} - \frac{1}{b} \right)
\]
and then the capacitance
\[
    C = \frac{Q}{V} = 4\pi\epsilon_0 \frac{ab}{b - a}
\]

\separator%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch02-capacitor_energy-01}
    \caption{``Charging up'' a capacitor.}%
\end{figure}
\begin{itemize}
    \item To ``charge up'' the capacitor, you have to remove electrons from the positive to negative plates (through some sort of circuit).

    \item Suppose that at some intermediate stage, the charge on the positive plate is $q$.
        The potential difference in this moment reads
        \[
            V = \frac{q}{C}
        \]

    \item Since work you need to carry charge $Q$ across potential difference $\Delta V$ is $W = Q\Delta V$, the work you must do to carry the next piece of charge $dq$ is
        \[
            dW = Vdq = \frac{q}{C} dq
        \]

    \item The total work necessary to go from $q = 0$ to $q = Q$ is then
        \[
            W
            =
            \int_0^Q \frac{q}{C} dq
            =
            \frac{1}{2} \frac{Q^2}{C}
        \]

    \item In terms of the final potential difference $V$ (it is much easier to measure $V$ than $Q$), energy stored in the capacitor is
        \begin{equation}
            W = \frac{1}{2} C V^2
        \end{equation}
\end{itemize}

\framed{%
    Homework problems: 2.43
}
