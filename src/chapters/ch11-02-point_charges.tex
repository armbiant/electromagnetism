\section{Point Charges}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Power Radiated by a Point Charge}
In Chapter 10, we derived the fields of a point charge $q$ in arbitrary motion
\begin{equation}
    \mathbf E(\mathbf r, t)
    =
    \frac{q}{4\pi\epsilon_0} \frac{\eta}{(\spvec \cdot \mathbf u)^3}
    [
        \underbrace{(c^2 - v^2)\mathbf u}_{\color{blue}\text{velocity field}}
        +
        \underbrace{\spvec \times (\mathbf u \times \mathbf a)}_{\color{blue}\text{acceleration field}}
    ]
\end{equation}
where $\mathbf u = c\uspvec - \mathbf v$, and
\begin{equation}
    \mathbf B(\mathbf r, t)
    =
    \frac{1}{c} \uspvec \times \mathbf E(\mathbf r, t)
\end{equation}
The first term in $\mathbf E$ is the velocity field and the second is the acceleration field.

The Poynting vector is
\begin{equation}
    \mathbf S
    =
    \frac{1}{\mu_0} (\mathbf E \times \mathbf B)
    =
    \frac{1}{\mu_0 c} \mathbf E \times (\uspvec \times \mathbf E)
    =
    \frac{1}{\mu_0 c} \left( E^2\uspvec - (\uspvec \cdot \mathbf E)\mathbf E \right)
\end{equation}
Not all of this energy flux constitutes radiation; some of it is just \textit{field energy} carried along by the particle as it moves.
\framed{%
    The radiation energy is the stuff that detaches itself from the charge and propagates off to infinity.
}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch11-point_charge_trajectory-01}
    \caption{Point charge trajectory.}%
\end{figure}
To calculate the total power radiated by the particle at time $t_r$, we draw a large sphere of radius $\eta$ centered at the position of the particle at time $t_r$ and integrate the Poynting vector over the surface.
Of course, we must account for the time it takes for the radiation to reach the sphere:
\begin{equation}
    t - t_r = \frac{\eta}{c}
\end{equation}
As before, we pick out any terms in $\mathbf S$ that go like $1/\eta^2$, which will yields a finite value.
Therefore, only the acceleration fields (or \textbf{radiation fields}) represent true radiation:
\begin{equation}
    \mathbf E_{\rm rad}
    =
    \frac{q}{4\pi\epsilon_0}
    \frac{\eta}{(\spvec \cdot \mathbf u)^3}
    (\spvec \times (\mathbf u \times \mathbf a))
\end{equation}
Since $\mathbf E_{\rm rad}$ is perpendicular to $\spvec$, the radiation part of the Poynting vector reads
\begin{equation}
    \mathbf S_{\rm rad} = \frac{1}{\mu_0 c} E_{\rm rad}^2 \uspvec
\end{equation}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch11-point_charge_radiation_flux-01}
    \caption{Radiation flux iso-surface of point charge at rest.}%
\end{figure}
Suppose that the charge was instantaneously \textit{at rest} at time $t_r$ ($\mathbf v = 0$ and $\mathbf a \ne 0$).
Then, $\mathbf u = c\uspvec$ and
\begin{equation}
    \mathbf E_{\rm rad}
    =
    \frac{q}{4\pi\epsilon_0 c^2} \frac{1}{\eta} (\uspvec \times (\uspvec \times \mathbf a))
    =
    \frac{\mu_0}{4\pi} \frac{q}{\eta}
    \left( (\uspvec \cdot \mathbf a)\uspvec - \mathbf a \right)
\end{equation}
The Poynting vector becomes
\begin{equation}
    \mathbf S_{\rm rad}
    =
    \frac{1}{\mu_0 c}
    \left( \frac{\mu_0}{4\pi} \frac{q}{\eta} \right)^2
    \left( a^2 - (\uspvec \cdot \mathbf a)^2 \right)
    \uspvec
    =
    \frac{\mu_0 q^2 a^2}{16 \pi^2 c}
    \left( \frac{\sin^2\theta}{\eta^2} \right)
    \uspvec
\end{equation}
where $\theta$ is the angle between $\mathbf a$ and $\uspvec$.
The maximum power is emitted perpendicular to $\mathbf a$; no power is radiated along the direction of $\mathbf a$.

The total power radiated is
\begin{equation}
    P
    =
    \oint \mathbf S \cdot d\mathbf a
    =
    \frac{\mu_0 q^2 a^2}{16 \pi^2 c}
    \underbrace{%
        \oint \frac{\sin^2\theta}{\cancel{\eta^2}} \cdot \cancel{\eta^2}\sin\theta d\theta d\phi
    }_{\color{blue}\leftarrow \int_0^\pi \sin^3\theta d\theta = 4/3}
    =
    \frac{\mu_0 q^2 a^2}{6 \pi c}
\end{equation}
This is the \textbf{Larmor formula} for a point charge instantaneously at rest.
\\

Although we derived them on the assumption that $v = 0$, the expressions for $\mathbf S_{\rm rad}$ and $P$ hold to a good approximation as long as $v \ll c$.

\framed{%
    Homework problems: 11.4
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Radiation Reaction}
\begin{itemize}
    \item The radiation carries off energy, which came ultimately at the expense of particle's kinetic energy.
    \item Under the influence of a given force, therefore, a charged particle accelerates less than a neutral one of the same mass.
    \item The radiation exerts a force ($\mathbf F_{\rm rad}$) back on the charge---a recoil force called the \textbf{radiation reaction} force.
\end{itemize}
We will derive it by invoking conservation of energy.
\\

For a nonrelativistic particle ($v \ll c$), the total power radiated is given by the Larmor formula:
\begin{equation}
    P = \frac{\mu_0 q^2 a^2}{6\pi c}
\end{equation}
By conservation of energy, this is also the rate at which the particle loses energy, under the influence of the radiation reaction force:
\begin{equation}
    \mathbf F_{\rm rad} \cdot \mathbf v
    =
    -\frac{\mu_0 q^2 a^2}{4\pi c}
    {\color{blue}\,\ll\,0}
\end{equation}

\framed{%
    \color{gray}
    Technically, the velocity fields also carry energy; they just don't transport it out to infinity.
    As the particle moves around, energy is exchanged between it and the velocity fields, as well as energy is irreversibly radiated away by the acceleration fields.
    The above expression of power accounts only for the latter.
}

To simplify the problem, let's consider only intervals over which the system returns to its initial state (for example, periodic motion).
Then, since the configuration becomes identical to its initial state, the energy of the velocity fields is the same at both ends, and the only net loss is in the form of radiation.

Then, the energy lost through the radiation reaction over the interval is
\begin{equation}
    \int_{t_1}^{t_2} \mathbf F_{\rm rad} \cdot \mathbf v dt
    =
    -\frac{\mu_0 q^2}{6\pi c} \int_{t_1}^{t_2} a^2 dt
    \label{eq:ch11:EnergyLostThroughRadiationReaction}
\end{equation}
understanding that the state of the system is identical at $t_1$ and $t_2$.
Integrating by parts, the right side of the equation reads
\[
    \int_{t_1}^{t_2} a^2 dt
    =
    \int_{t_1}^{t_2} \left( \frac{d\mathbf v}{dt} \right) \cdot \left( \frac{d\mathbf v}{dt} \right) dt
    =
    \underbrace{\left.\mathbf v \cdot \left( \frac{d\mathbf v}{dt} \right)\right|_{t_1}^{t_2}}_{\color{blue}=0}
    -
    \int_{t_1}^{t_2} \mathbf v \cdot \frac{d^2\mathbf v}{dt^2} dt
\]
So, Eq.~(\ref{eq:ch11:EnergyLostThroughRadiationReaction}) becomes
\begin{equation}
    \int_{t_1}^{t_2}
    \left( \mathbf F_{\rm rad} - \frac{\mu_0 q^2}{6\pi c} \dot{\mathbf a} \right)
    \cdot \mathbf v dt
    = 0
    \label{eq:ch11:AverageRadiationEnergyLoss}
\end{equation}
This is certainly satisfied if
\begin{equation}
    \mathbf F_{\rm rad} = \frac{\mu_0 q^2}{6\pi c} \dot{\mathbf a}
    \label{eq:ch11:AbrahamLorentzFormula}
\end{equation}
This is the \textbf{Abraham-Lorentz formula} for the radiation reaction force.
\\

Of course, Eq.~(\ref{eq:ch11:AverageRadiationEnergyLoss}) does not prove Eq.~(\ref{eq:ch11:AbrahamLorentzFormula}) (e.g., it does not say anything about the perpendicular component of $\mathbf F_{\rm rad}$), but it represents the simplest form that the radiation reaction force could take, consistent with conservation of energy.
