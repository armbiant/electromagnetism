\section{Linear and Nonlinear Media}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Magnetic Susceptibility and Permeability}
\begin{itemize}
    \item In paramagnetic and diamagnetic materials, the magnetization is sustained by the field $\mathbf B$; when $\mathbf B$ is removed, $\mathbf M$ disappears.

    \item For most substances, the magnetization is proportional to the field, provided the field is not too strong.

    \item Customarily, the relation is written in terms of $\mathbf H$:
        \begin{equation}
            \boxed{%
                \mathbf M = \chi_m \mathbf H
            }
            \label{eq:ch06:LinearMedia}
        \end{equation}

        \item The constant of proportionality $\chi_m$ is called the \textbf{magnetic susceptibility}; this is dimensionless---\textit{positive} for paramagnets and \textit{negative} for diamagnets.

        \item Materials that obey Eq.~(\ref{eq:ch06:LinearMedia}) are called \textbf{linear media}.
\end{itemize}

The magnetic field in the presence of linear media is
\begin{equation}
    \mathbf B = \mu_0 (\mathbf H + \mathbf M)
    = \underbrace{\mu_0 (1 + \chi_m)}_{\color{blue}\mu} \mathbf H
\end{equation}
Thus, $\mathbf B$ is also proportional to $\mathbf H$:
\begin{equation}
    \mathbf B = \mu \mathbf H
\end{equation}
where
\begin{equation}
    \mu \equiv \mu_0 (1 + \chi_m)
\end{equation}
called the \textbf{permeability} of the material.
Of course, in vacuum, $\chi_m = 0$.

\framed{%
    \example{6.3}%
    An infinite solenoid ($n$ turns per unit length, current $I$) is filled with linear material of $\chi_m$.
    Find the magnetic field inside.
}
\solution%

Leveraging cylindrical symmetry and using Amp\`ere's methods, we have
\[
    \mathbf H = n I \uvec z
\]
In terms of $\mathbf B$,
\[
    \mathbf B = \mu_0 (1 + \chi_m) \mathbf H = \mu_0 (1 + \chi_m) n I \uvec z
\]
If the medium is paramagnetic (so that $\chi_m > 0$), then the field is slightly \textit{enhanced}.
If it is diamagnetic (so that $\chi_m < 0$), the field is somewhat \textit{reduced}.

The bound surface current is
\[
    \mathbf K_b = \mathbf M \times \uvec n
    = \chi_m (\mathbf H \times \uvec s)
    = \chi_m n I \uvec \phi
\]

\separator%

The volume bound current density in a homogeneous linear material is proportional to the free current density:
\begin{equation}
    \mathbf J_b = \nabla \times \mathbf M = \nabla \times (\chi_m \mathbf M) = \chi_m \mathbf J_f
\end{equation}
In particular, unless free current actually flows through the material, all bound current will be at the surface.

\framed{%
    Homework problems: 6.16, 6.17, 6.18
}
