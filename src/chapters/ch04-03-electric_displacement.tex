\section{Electric Displacement}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Gauss's Law in the Presence of Dielectrics}
\begin{itemize}
    \item We found that the effect of polarization is to produce accumulations of (bound) charge, $\rho_b = -\nabla \cdot \mathbf P$ and $\sigma_b = \mathbf P \cdot \uvec n$.
    \item We want to know the field attributable to bound charge \textit{plus} the field due to everything else (i.e., by \textit{free charge} $\rho_f$).
\end{itemize}

Within the dielectric, the total charge density can be written as
\begin{equation}
    \rho = \rho_b + \rho_f
\end{equation}
Then, Gauss's law reads
\[
    \epsilon_0 \nabla \cdot \mathbf E
    =
    \rho = \rho_b + \rho_f
    =
    -\nabla \cdot \mathbf P + \rho_f
\]
where $\mathbf E$ is now the \textit{total field}.

Combining the two divergence terms,
\[
    \nabla \cdot (\epsilon_0 \mathbf E + \mathbf P) = \rho_f
\]
We designate the term in the parentheses as
\begin{equation}
    \boxed{%
        \mathbf D = \epsilon_0 \mathbf E + \mathbf P
    }
    \label{eq:ch04:ElectricDisplacement}
\end{equation}
which is known as the \textbf{electric displacement}.
In terms of $\mathbf D$, Gauss's law reads
\begin{equation}
    \boxed{%
        \nabla \cdot \mathbf D = \rho_f
    }
    \label{eq:ch04:GaussLaw:D}
\end{equation}
or in integral form,
\begin{equation}
    \oint \mathbf D \cdot d\mathbf a = Q_{f_{\rm enc}}
    \label{eq:ch04:GaussLaw:D:IntegralForm}
\end{equation}
where $Q_{f_{\rm enc}}$ denotes the total \textit{free} charge enclosed in the volume.

Note that this Gauss's law \textit{makes reference only to free charges}; we do not need to know $\mathbf P$.

\framed{%
    \example{4.4}%
    A long straight wire, carrying uniform line charge $\lambda$, is surrounded by rubber insulation out to radius $a$.
    Find the electric displacement.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-example_4-01}
    \caption{Example 4.4 Electric displacement of a long straight wire.}%
\end{figure}
Cylindrical symmetry demands $\mathbf D = D(s) \uvec s$.
Applying Gauss's law on a cylindrical Gaussian surface,
\[
    (2\pi L) D = \lambda L
\]
Therefore,
\[
    \mathbf D = \frac{\lambda}{2\pi s} \uvec s
\]
Outside the insulation, $\mathbf P = 0$, so
\[
    \mathbf E = \frac{\mathbf D}{\epsilon_0} = \frac{\lambda}{2\pi\epsilon_0 s} \uvec s
    ,\qquad s > a
\]
Inside the rubber, $\mathbf P$ is unknown, and we cannot determine $\mathbf E$.

\framed{%
    Homework problems: 4.15, 4.16a
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{A Deceptive Parallel}
Eq.\ (\ref{eq:ch04:GaussLaw:D}) looks like Gauss's law of electrostatic fields ($\nabla \cdot \mathbf E = \rho/\epsilon_0$), only the total charge density $\rho$ is replaced by the free charge density $\rho_f$, and $\mathbf D$ is substituted for $\epsilon_0 \mathbf E$.

However, the similarity ends here.
There is no Coulomb's law for $\mathbf D$:
\[
    \mathbf D(\mathbf r)
    \ne
    \frac{1}{4\pi} \int \frac{\uspvec}{\eta^2} \rho_f(\mathbf r') d\tau'
\]
The divergence alone is insufficient to determine a vector field; you need to know the curl as well which is not always zero ($\because$ no Coulomb's law for $\mathbf D$):
\begin{equation}
    \nabla \times \mathbf D
    =
    \epsilon_0 \underbrace{\nabla \times \mathbf E}_{\color{blue} = 0}
    +
    \nabla \times \mathbf P
    =
    \nabla \times \mathbf P
\end{equation}
There is no reason, in general, to suppose that $\nabla \times \mathbf P$ vanishes.\\

Because $\nabla \times \mathbf D \ne 0$, there is, in general, no potential for $\mathbf D$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Boundary Conditions}
The Gauss's law of $\mathbf D$ tells us the discontinuity in the component perpendicular to an interface
\begin{equation}
    D_{\rm above}^{\perp} - D_{\rm below}^{\perp} = \sigma_f
    \label{eq:ch04:BoundaryCondition:DPerp}
\end{equation}
The curl of $\mathbf D$ gives the discontinuity in the parallel components
\begin{equation}
    \mathbf D_{\rm above}^{\|}
    -
    \mathbf D_{\rm below}^{\|}
    =
    \mathbf P_{\rm above}^{\|}
    -
    \mathbf P_{\rm below}^{\|}
\end{equation}
