\section{Point Charges}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Li\'enard-Wiechert Potentials}
Let's calculate the retarded potentials, $V(\mathbf r, t)$ and $\mathbf A(\mathbf r, t)$, of a point charge $q$ moving on a specific trajectory
\begin{equation}
    \mathbf w(t) = \text{position of $q$ at time $t$}
\end{equation}
Recall the formula for the scalar potential (Eq.~\ref{eq:ch10:RetardedPotentials})
\begin{equation}
    V(\mathbf r, t)
    =
    \frac{1}{4\pi\epsilon_0} \int \frac{\rho(\mathbf r', t_r)}{\eta} d\tau'
\end{equation}
A na\"ive substitution of $\rho(\mathbf r', t_r) = q \delta^3(\mathbf r' - \mathbf w(t_r))$ may suggest that the potential is simply
\[
    \frac{1}{4\pi\epsilon_0} \frac{q}{|\mathbf r - \mathbf w(t_r)|}
\]
But this is wrong because $\rho$ depends on $t_r$ which is a function of $\mathbf r'$.
That is, the total charge is not what you would think
\begin{equation}
    \int \rho(\mathbf r', t_r) d\tau'
    =
    \frac{q}{1 - \uspvec \cdot \mathbf v/c} \ne q
    \label{eq:ch10:TotalChargeOfMovingQ}
\end{equation}
where $\spvec = \mathbf r - \mathbf w(t_r)$ and $\mathbf v = \mathbf v(t_r) = \frac{d\mathbf w(t_r)}{dt}$ for the latter case.
\\

The fact that the charge of a moving particle looks different is a purely geometric effect as a result of the charge density being the point charge divided by volume.
(We are not talking about special relativity here.)

For geometrical interpretation of this phenomenon, refer to the textbook.

\separator%

\textbf{Proof of Eq.~(\ref{eq:ch10:TotalChargeOfMovingQ})}
\\
Recall the delta function property:
\[
    \int f(x) \underbrace{\delta(g(x))}_{\color{blue}(\dagger)} dx
    =
    \sum_i \int f(x_i) \underbrace{\frac{\delta(x - x_i)}{|g'(x_i)|}}_{\color{blue}(\dagger\dagger)} dx
    =
    \sum_i \frac{f(x_i)}{|g'(x_i)|}
\]
where $g'(x) = dg/dx$ and $x_i$'s are all solutions of $g(x) = 0$.
From this we find that $(\dagger) = (\dagger\dagger)$.

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch10-moving_charge_geometry-01}
    \caption{Moving charge configurations.}%
    \label{fig:ch10:MovingChargeConfiguration}
\end{figure}
We choose our coordinate system (Figure~\ref{fig:ch10:MovingChargeConfiguration}) such that the field point is at the origin and the $x$ direction is along the line of sight to the charge $q$.

The retarded time at point $\mathbf r'$ is
\begin{equation}
    t_r(\mathbf r') = t - \frac{r'}{c}
\end{equation}
where $r' = |\mathbf r'|$.
Taking the gradient (in terms of $\mathbf r'$)
\begin{equation}
    \nabla' t_r = -\nabla' \left( \frac{\mathbf r'}{c} \right) = -\frac{\uvec r}{c}
\end{equation}
In coordinate-independent form, this reads
\begin{equation}
    \nabla' t_r = \frac{\uspvec}{c}
\end{equation}
where $\spvec = \mathbf r - \mathbf r'$.
\\

\textbf{(1) Motion Parallel To Line of Sight.}
First, let's consider the case where $q$ is moving \textit{parallel} to the line of sight (i.e., $\mathbf v = v\uvec x$; Figure~\ref{fig:ch10:MovingChargeConfiguration}a).
The volume integral of the 3D delta function is
\[
    \int \delta^3(\mathbf r' - \mathbf w(t_r)) d\tau'
    =
    \iiint \delta(x' - w_x(t_r)) \delta(y' - w_y(t_r)) \delta(z' - w_z(t_r)) dx' dy' dz'
\]
where $t_r$ is a function of $\mathbf r'$.
The solution of the equation with the delta function argument
\[
    \mathbf r' - \mathbf w(t_r) = 0
\]
is, of course,
\begin{equation}
    \left\{
        \begin{aligned}
            x_0' &= w_x(t_r^0) \\
            y_0' &= w_y(t_r^0) = 0 \\
            z_0' &= w_z(t_r^0) = 0 \\
        \end{aligned}
    \right.
\end{equation}
where $t_r^0$ is the retarded time evaluated at the position of the particle
\begin{equation}
    t_r^0 = t - \frac{w}{c}
\end{equation}
Using the delta function identity,
\[
    \left\{
        \begin{aligned}
            \delta(x' - w_x(t_r))
            &=
            \frac{\delta(x' - x_0')}{\left|1 - \frac{dw_x}{dt_r}\frac{\partial t_r}{\partial x'}\right|_{x' = x_0'}}
            =
            \frac{\delta(x' - x_0')}{1 - (v)\left( -\frac{1}{c} \right)}
            \\
            \delta(y' - w_y(t_r))
            &=
            \frac{\delta(y' - y_0')}{\left|1 - \frac{dw_y}{dt_r}\frac{\partial t_r}{\partial y'}\right|_{y' = y_0'}}
            =
            \frac{\delta(y')}{1 - (0)(0)}
            \\
            \delta(z' - w_z(t_r))
            &=
            \ \quad\qquad\cdots\qquad\quad\ %
            =
            \frac{\delta(z')}{1 - (0)(0)}
        \end{aligned}
    \right.
\]
where $v$ is the particle speed at $t_r$.
Substitution of them yields
\begin{equation}
    \int \delta^3(\mathbf r' - \mathbf w(t_r)) d\tau'
    =
    \frac{1}{1 + v/c}
\end{equation}

Therefore, a point charge moving away from the line of sight appears to have a total charge smaller by a factor $(1 + v/c)^{-1}$.
Likewise, an approaching charge appears to have a larger charge by a factor $(1 - v/c)^{-1}$.
\\

\textbf{(2) Motion Perpendicular To Line of Sight.}
Now, let's consider $q$ is moving perpendicular to the line of sight (i.e., $\mathbf v = v\uvec y$; Figure~\ref{fig:ch10:MovingChargeConfiguration}b).
Similarly, the 1D delta functions can be replaced with
\[
    \left\{
        \begin{aligned}
            \delta(x' - w_x(t_r))
            &=
            \frac{\delta(x' - x_0')}{\left|1 - \frac{dw_x}{dt_r}\frac{\partial t_r}{\partial x'}\right|_{x' = x_0'}}
            =
            \frac{\delta(x' - x_0')}{1 - (0)\left( -\frac{1}{c} \right)}
            \\
            \delta(y' - w_y(t_r))
            &=
            \frac{\delta(y' - y_0')}{\left|1 - \frac{dw_y}{dt_r}\frac{\partial t_r}{\partial y'}\right|_{y' = y_0'}}
            =
            \frac{\delta(y')}{1 - (v)(0)}
            \\
            \delta(z' - w_z(t_r))
            &=
            \ \quad\qquad\cdots\qquad\quad\ %
            =
            \frac{\delta(z')}{1 - (0)(0)}
        \end{aligned}
    \right.
\]
So, the volume integral yields
\begin{equation}
    \int \delta^3(\mathbf r' - \mathbf w(t_r)) d\tau' = 1
\end{equation}

Therefore, the apparent charge of a particle moving perpendicular to the line of sight is identical.
\\

\textbf{(3) Motion Arbitrary To Line of Sight.}
Consequently, it is only the line-of-sight motion that affects the apparent charge.
Since $-\mathbf v \cdot \uspvec$ is the line-of-sight velocity away from the field point, the coordinate-independent generalization yields
\begin{equation}
    \int \delta^3(\mathbf r' - \mathbf w(t_r)) d\tau'
    =
    \left( 1 - \frac{\mathbf v \cdot \uspvec}{c} \right)^{-1}
\end{equation}

\separator%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch10-moving_charge_geometry-02}
    \caption{Moving charge.}%
\end{figure}
For the point charge, rearranging the retarded time, we get
\begin{equation}
    t_r = t - \frac{|\mathbf r - \mathbf w(t_r)|}{c}
    \quad\rightarrow\quad
    |\mathbf r - \mathbf w(t_r)| = c(t - t_r)
    \label{eq:ch10:RetardedTime:PointCharge}
\end{equation}
The left side is the distance the ``news'' must travel, and $\Delta t = t - t_r$ is the time it takes to make the trip.

It is important to note that at most \text{one} point on the trajectory is ``in communication'' with $\mathbf r$ at any particular time $t$.
That is, \textit{only one retarded point contributes to the potentials, at any given moment}.
\\

The scalar potential for the point charge is given by
\begin{equation}
    \boxed{%
        V(\mathbf r, t)
        =
        \frac{1}{4\pi\epsilon_0} \frac{q}{\eta} \left( 1 - \frac{\mathbf v \cdot \uspvec}{c} \right)^{-1}
        =
        \frac{1}{4\pi\epsilon_0} \frac{qc}{\eta c - \mathbf v \cdot \spvec}
    }
    \label{eq:ch10:MovingPointChargeV}
\end{equation}
where $\mathbf v = \frac{d\mathbf w(t_r)}{dt}$ (evaluated at the retarded time) and $\spvec = \mathbf r - \mathbf w(t_r)$.
Making use of the current density of the point charge
\[
    \mathbf J(\mathbf r, t) = \rho(\mathbf r, t) \mathbf v(t)
\]
the vector potential reads
\[
    \begin{aligned}
        \mathbf A(\mathbf r, t)
        &=
        \frac{\mu_0}{4\pi} \int \frac{\rho(\mathbf r', t_r) \mathbf v(t_r)}{\eta} d\tau'
        \\
        &=
        \frac{\mu_0}{4\pi} \int \frac{\mathbf v}{\eta} q\delta^3(\mathbf r' - \mathbf w(t_r)) d\tau'
        \\
        &=
        \frac{\mu_0}{4\pi} \frac{q\mathbf v}{\eta} \left( 1 - \frac{\mathbf v \cdot \uspvec}{c} \right)^{-1}
    \end{aligned}
\]
or by making use of $\mu_0\epsilon_0 = 1/c^2$
\begin{equation}
    \boxed{%
        \mathbf A(\mathbf r, t)
        =
        \frac{\mu_0}{4\pi} \frac{qc \mathbf v}{\eta c - \mathbf v \cdot \spvec}
        =
        \frac{\mathbf v}{c^2} V(\mathbf r, t)
    }
    \label{eq:ch10:MovingPointChargeA}
\end{equation}
Note that $\mathbf v$ and $\eta$ are evaluated at the retarded time $t_r$.
Eqs.~(\ref{eq:ch10:MovingPointChargeV}) and~(\ref{eq:ch10:MovingPointChargeA}) are the famous \textbf{Li\'enard-Wiechert potentials} for a moving point charge.

\framed{%
    \example{10.3}%
    Find the potentials of a point charge moving with constant velocity.
}
\solution%

The trajectory of this particle is simply
\[
    \mathbf w(t) = \int_0^t \mathbf v dt = \mathbf v t + \mathbf w_0
\]
where $\mathbf w_0 = \mathbf w(0)$.
Let's choose the origin such that $\mathbf w_0 = 0$.
\\

The right side of the Li\'enard-Wiechert potentials is defined at the retarded time.
To get it, we use Eq.~(\ref{eq:ch10:RetardedTime:PointCharge})
\[
    |\mathbf r - \mathbf v t_r| = c(t - t_r)
\]
Squaring both sides,
\[
    \begin{aligned}
        &c^2 (t^2 - 2t t_r - t_r^2) = r^2 - 2 (\mathbf r \cdot \mathbf v) t_r - v^2 t_r^2
        \\
        &\ \rightarrow\ %
        (c^2 - v^2) t_r^2 + 2(c^2 t - \mathbf r \cdot \mathbf v) t_r + (r^2 - c^2 t^2) = 0
    \end{aligned}
\]
Solving for $t_r$,
\begin{equation}
    t_r
    =
    \frac{(c^2 t - \mathbf r \cdot \mathbf v) \pm \sqrt{(c^2 t - \mathbf r \cdot \mathbf v)^2 + (c^2 - v^2)(r^2 - c^2 t^2)}}{c^2 - v^2}
\end{equation}
The sign must be chosen to ensure causality, $t \ge t_r$.
In the limit $v \rightarrow 0$,
\[
    t_r = \frac{c^2 t \pm cr}{c^2} = t \pm \frac{r}{c}
\]
Evidently, we want the minus sign.
\\

Making use of
\[
    \eta = c(t - t_r)
    ,\quad
    \spvec = \mathbf r - \mathbf v t_r
\]
we evaluate
\[
    \begin{aligned}
        \eta c - \spvec \cdot \mathbf v
        &=
        c^2(t - t_r) - \mathbf r \cdot \mathbf v + v^2 t_r
        \\
        &=
        (c^2 t - \mathbf r \cdot \mathbf v) - (c^2 - v^2) t_r
    \end{aligned}
\]
Substituting $t_r$, we get
\[
    \eta c - \mathbf \spvec \cdot \mathbf v
    =
    \sqrt{(c^2 t - \mathbf r \cdot \mathbf v)^2 + (c^2 - v^2)(r^2 - c^2 t^2)}
\]
Therefore, the potentials become
\begin{equation}
    V(\mathbf r, t)
    =
    \frac{1}{4\pi\epsilon_0}
    \frac{qc}{\sqrt{(c^2 t - \mathbf r \cdot \mathbf v)^2 + (c^2 - v^2)(r^2 - c^2 t^2)}}
    \label{eq:ch10:MovingPointChargeV:ConstantVelocity1}
\end{equation}
and
\begin{equation}
    \mathbf A(\mathbf r, t)
    =
    \frac{\mu_0}{4\pi}
    \frac{qc\mathbf v}{\sqrt{(c^2 t - \mathbf r \cdot \mathbf v)^2 + (c^2 - v^2)(r^2 - c^2 t^2)}}
    \label{eq:ch10:MovingPointChargeA:ConstantVelocity}
\end{equation}

\framed{%
    Homework problems: 10.15, 10.16
}

\framed{%
    \problem{10.16}%
    Show that the scalar potential of a point charge moving with constant velocity (Eq.~\ref{eq:ch10:MovingPointChargeV:ConstantVelocity1}) can be written more simply as
    \begin{equation}
        V(\mathbf r, t)
        =
        \frac{1}{4\pi\epsilon_0} \frac{q}{R \sqrt{1 - v^2 \sin^2\theta/c^2}}
    \end{equation}
    where $\mathbf R \equiv \mathbf r - \mathbf v t$ is the vector from the \textit{present} position of the particle to the field point $\mathbf r$, and $\theta$ is the angle between $R$ and $v$.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch10-problem_16-01}
    \caption{Problem 10.16 Geometry of $R$ and $v$.}%
\end{figure}
Let's expand the quantity inside the square root in Eqs.\ (\ref{eq:ch10:MovingPointChargeV:ConstantVelocity1}) and\ (\ref{eq:ch10:MovingPointChargeA:ConstantVelocity}):
\[
    \begin{aligned}
        &(c^2 t - \mathbf r \cdot \mathbf v)^2
        +
        (c^2 - v^2)(r^2 - c^2 t^2)
        \\
        &=
        \cancel{c^4t^2} - 2c^2t(\mathbf r \cdot \mathbf v) + (\mathbf r \cdot \mathbf v)^2
        \\
        &\quad+
        c^2r^2 - \cancel{c^4t^2} - r^2v^2 + c^2t^2v^2
        \\
        &=
        \underbrace{(c^2r^2 - 2c^2(\mathbf r \cdot \mathbf v)t + c^2v^2t^2)}_{\color{blue}(1)}
        -
        \underbrace{(r^2v^2 - (\mathbf r \cdot \mathbf v)^2)}_{\color{blue}(2)}
    \end{aligned}
\]
The first term can be easily recognized:
\[
    {\color{blue}(1)}
    =
    c^2 (\mathbf r - \mathbf v t)^2
    =
    c^2 R^2
\]
For the second term, we make use of $\mathbf r = \mathbf R + \mathbf v t$ to get
\[
    (\mathbf r \cdot \mathbf v)^2
    =
    (\mathbf R \cdot \mathbf v)^2 + 2v^2t(\mathbf R \cdot \mathbf v) + v^4t^2
\]
and
\[
    r^2v^2 = R^2v^2 + 2tv^2(\mathbf R \cdot \mathbf v) + v^4t^2
\]
Therefore,
\[
    \begin{aligned}
        {\color{blue}(2)}
        &=
        R^2v^2 + \cancel{2tv^2(\mathbf R \cdot \mathbf v)} + \cancel{v^4t^2}
        \\
        &\quad -
        (\mathbf R \cdot \mathbf v)^2 - \cancel{2tv^2(\mathbf R \cdot \mathbf v)} - \cancel{v^4t^2}
        \\
        &=
        R^2v^2 (1 - \cos^2\theta)
        \\
        &=
        R^2v^2 \sin^2\theta
    \end{aligned}
\]
Putting them, we get
\[
    {\color{blue}(1)} - {\color{blue}(2)}
    =
    c^2R^2 - R^2v^2\sin^2\theta
    =
    c^2R^2 \left( 1 - \frac{v^2\sin^2\theta}{c^2} \right)
\]

\separator%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Fields of a Moving Point Charge}
Having obtained the Li\'enard-Wiechert potentials
\begin{equation}
    V(\mathbf r, t)
    =
    \frac{1}{4\pi\epsilon_0} \frac{qc}{\eta c - \mathbf v \cdot \spvec}
    ,\quad
    \mathbf A(\mathbf r, t)
    =
    \frac{\mathbf v}{c^2} V(\mathbf r, t)
\end{equation}
we can now calculate the electric and magnetic fields
\[
    \mathbf E = -\nabla V - \frac{\partial\mathbf A}{\partial t}
    ,\quad
    \mathbf B = \nabla \times \mathbf A
\]
keeping in mind that the potentials involve the retarded time which is a function of $\mathbf r$ and $t$ and the solution of the nonlinear equation
\begin{equation}
    \eta = |\mathbf r - \mathbf w(t_r)| = c(t - t_r)
    \label{eq:ch10:RetardedTime:PointCharge:1}
\end{equation}

Let's begin with $\nabla V$:
\begin{equation}
    \nabla V
    =
    \frac{qc}{4\pi\epsilon_0} \frac{-1}{(\eta c - \spvec \cdot \mathbf v)^2}
    \underbrace{\nabla (\eta c - \spvec \cdot \mathbf v)}_{\color{blue}(\dagger)}
\end{equation}
Now it all boils down to calculating ($\dagger$).
From Eq.~(\ref{eq:ch10:RetardedTime:PointCharge:1}), it follows
\begin{equation}
    \nabla \eta = -c\nabla t_r
\end{equation}
For the second term of ($\dagger$), product rule 4 gives
\[
    \nabla(\spvec \cdot \mathbf v)
    =
    (\spvec \cdot \nabla)\mathbf v
    +
    (\mathbf v \cdot \nabla)\spvec
    +
    \spvec \times (\nabla \times \mathbf v)
    +
    \mathbf v \times (\nabla \times \spvec)
\]
Evaluating these terms one by one:
\[
    \begin{aligned}
        (\spvec \cdot \nabla)\mathbf v
        &=
        (\spvec \cdot \nabla)(v_x\uvec x) + \cdots
        \\
        &=
        \left(
            \uvec x (\spvec \cdot \nabla v_x)
            +
            v_x \underbrace{(\spvec \cdot \nabla)\uvec x}_{\color{blue} = 0}
        \right) + \cdots
        \\
        &=
        \uvec x \frac{dv_x}{dt_r} (\spvec \cdot \nabla t_r) + \cdots
        \\
        &=
        \mathbf a (\spvec \cdot \nabla t_r)
    \end{aligned}
\]
where $\mathbf a = \dot{\mathbf v}(t_r)$ is the acceleration of the particle at the retarded time.
The second term:
\[
    \begin{aligned}
        (\mathbf v \cdot \nabla)\spvec
        &=
        (\mathbf v \cdot \nabla)\mathbf r
        -
        (\mathbf v \cdot \nabla)\mathbf w
        \\
        &=
        \left( \uvec x(\mathbf v \cdot \nabla x) + \cdots \right)
        -
        \left( \uvec x\frac{dw_x}{dt_r} (\mathbf v \cdot \nabla t_r) + \cdots \right)
        \\
        &=
        \mathbf v - \mathbf v(\mathbf v \cdot \nabla t_r)
    \end{aligned}
\]
The third term:
\[
    \nabla \times \mathbf v(t_r)
    =
    \nabla t_r \times \frac{d\mathbf v}{dt_r}
    =
    \nabla t_r \times \mathbf a
\]
and thus
\[
    \spvec \times (\nabla \times \mathbf v)
    =
    (\spvec \cdot \mathbf a)\nabla t_r
    -
    (\spvec \cdot \nabla t_r)\mathbf a
\]
The last term:
\[
    \nabla \times \spvec
    =
    \underbrace{\nabla \times \mathbf r}_{\color{blue}=0}
    -
    \nabla \times \mathbf w(t_r)
    =
    - \nabla t_r \times \mathbf v
\]
and thus
\[
    \mathbf v \times (\nabla \times \spvec)
    =
    \mathbf v \times (\mathbf v \times \nabla t_r)
    =
    (\mathbf v \cdot \nabla t_r)\mathbf v
    -
    v^2 \nabla t_r
\]
Putting this all back to $(\dagger)$,
\[
    \begin{aligned}
        \nabla (\spvec \cdot \mathbf v)
        &=
        \cancel{\mathbf a(\spvec \cdot \nabla t_r)}
        +
        \mathbf v
        -
        \cancel{\mathbf v(\mathbf v \cdot \nabla t_r)}
        \\
        &\quad +
        (\spvec \cdot \mathbf a)\nabla t_r
        -
        \cancel{(\spvec \cdot \nabla t_r)\mathbf a}
        \\
        &\quad +
        \cancel{(\mathbf v \cdot \nabla t_r)\mathbf v}
        -
        v^2\nabla t_r
        \\
        &=
        \mathbf v + (\spvec \cdot \mathbf a - v^2)\nabla t_r
    \end{aligned}
\]
The gradient of $V$ then becomes
\begin{equation}
    \begin{aligned}
        \nabla V
        &=
        \frac{qc}{4\pi\epsilon_0} \frac{1}{(\eta c - \spvec \cdot \mathbf v)^2}
        \nabla (\spvec \cdot \mathbf v - \eta c)
        \\
        &=
        \frac{qc}{4\pi\epsilon_0} \frac{1}{(\eta c - \spvec \cdot \mathbf v)^2}
        \left( \mathbf v + (\spvec \cdot \mathbf a - v^2)\nabla t_r + c^2\nabla t_r \right)
        \\
        &=
        \frac{qc}{4\pi\epsilon_0} \frac{1}{(\eta c - \spvec \cdot \mathbf v)^2}
        \left( \mathbf v + (c^2 - v^2 + \spvec \cdot \mathbf a)\nabla t_r \right)
    \end{aligned}
\end{equation}
To complete the calculation, we need to know $\nabla t_r$ given by
\[
    \nabla t_r = -\frac{1}{c}\nabla \eta
\]
Expanding $\nabla \eta$ and making use of product rule 4,
\[
    \nabla \eta
    =
    \nabla \sqrt{\spvec \cdot \spvec}
    =
    \frac{1}{2\sqrt{\spvec \cdot \spvec}}
    \nabla(\spvec \cdot \spvec)
    =
    \frac{1}{\eta}
    \left( (\spvec \cdot \nabla)\spvec + \spvec \times (\nabla \times \spvec) \right)
\]
The first term reduces to
\[
    (\spvec \cdot \nabla) \spvec
    =
    (\spvec \cdot \nabla)\mathbf r
    -
    (\spvec \cdot \nabla)\mathbf w(t_r)
    =
    \spvec - \mathbf v(\spvec \cdot \nabla t_r)
\]
and the second term
\[
    \spvec \times (\nabla \times \spvec)
    =
    \spvec \times (-\nabla \times \mathbf w(t_r))
    =
    \spvec \times (\mathbf v \times \nabla t_r)
    =
    (\spvec \cdot \nabla t_r)\mathbf v
    -
    (\spvec \cdot \mathbf v) \nabla t_r
\]
Putting them back into $\nabla \eta$,
\[
    \begin{aligned}
        \nabla \eta
        &=
        \frac{1}{\eta}
        \left(
            \spvec - \mathbf v(\spvec \cdot \nabla t_r)
            +
            (\spvec \cdot \nabla t_r)\mathbf v - (\spvec \cdot \mathbf v)\nabla t_r
        \right)
        \\
        &=
        \frac{1}{\eta} \left( \spvec - (\spvec \cdot \mathbf v)\nabla t_r \right)
    \end{aligned}
\]
Putting it into equation $\nabla \eta = -c\nabla t_r$ and solving it for $\nabla t_r$,
\begin{equation}
    \nabla t_r = -\frac{\spvec}{\eta c - \spvec \cdot \mathbf v}
\end{equation}
Incorporating this result, we get
\begin{equation}
    \nabla V
    =
    \frac{1}{4\pi\epsilon_0} \frac{qc}{(\eta c - \spvec \cdot \mathbf v)^3}
    \left(
        (\eta c - \spvec \cdot \mathbf v)\mathbf v
        -
        (c^2 - v^2 + \spvec \cdot \mathbf a)\spvec
    \right)
\end{equation}

Now, we turn our attention to the time derivative of $\mathbf A$
\begin{equation}
    \frac{\partial\mathbf A}{\partial t}
    =
    \frac{\partial}{\partial t} \left( \frac{\mathbf v}{c^2}V \right)
    =
    \frac{\mathbf v}{c^2}\frac{\partial V}{\partial t}
    +
    \frac{V}{c^2}\frac{\partial\mathbf v}{\partial t}
\end{equation}
Looking at the time derivative of $V$ (and noting that the independent variables are $(\mathbf r, t)$),
\begin{equation}
    \frac{\partial V}{\partial t}
    =
    \frac{qc}{4\pi\epsilon_0}
    \frac{-1}{(\eta c - \spvec \cdot \mathbf v)^2}
    \underbrace{\frac{\partial}{\partial t}(\eta c - \spvec \cdot \mathbf v)}_{\color{blue}(\dagger\dagger)}
\end{equation}
Evaluating the first term in $(\dagger\dagger)$,
\[
    \frac{\partial(\eta c)}{\partial t}
    =
    c\frac{\partial}{\partial t} \left( c(t - t_r) \right)
    =
    c^2 \left( 1 - \frac{\partial t_r}{\partial t} \right)
\]
For the second term of $(\dagger\dagger)$, we will need
\[
    \frac{\partial}{\partial t}\mathbf v(t_r)
    =
    \frac{d\mathbf v}{dt_r} \frac{\partial t_r}{\partial t}
    =
    \mathbf a \frac{\partial t_r}{\partial t}
\]
and
\[
    \frac{\partial\spvec}{\partial t}
    =
    \frac{\partial}{\partial t} \left( \mathbf r - \mathbf w(t_r) \right)
    =
    -\frac{d\mathbf w}{dt_r}\frac{\partial t_r}{\partial t}
    =
    -\mathbf v\frac{\partial t_r}{\partial t}
\]
Collecting them together,
\begin{equation}
    \begin{aligned}
        \frac{\partial V}{\partial t}
        &=
        \frac{qc}{4\pi\epsilon_0} \frac{1}{(\eta c - \spvec \cdot \mathbf v)^2}
        \left(
            (\spvec \cdot \mathbf a)\frac{\partial t_r}{\partial t}
            -
            (\mathbf v \cdot \mathbf v)\frac{\partial t_r}{\partial t}
            -
            c^2\left( 1 - \frac{\partial t_r}{\partial t} \right)
        \right)
        \\
        &=
        \frac{qc}{4\pi\epsilon_0} \frac{1}{(\eta c - \spvec \cdot \mathbf v)^2}
        \left(
            (c^2 - v^2 + \spvec \cdot \mathbf a)\frac{dt_r}{dt} - c^2
        \right)
    \end{aligned}
\end{equation}
Therefore, the first term of $\partial\mathbf A/\partial t$ becomes
\begin{equation}
    \frac{\mathbf v}{c^2}\frac{\partial V}{\partial t}
    =
    \frac{qc}{4\pi\epsilon_0} \frac{1}{(\eta c - \spvec \cdot \mathbf v)^2}
    \frac{\mathbf v}{c^2} \left(
        (c^2 - v^2 + \spvec \cdot \mathbf a)\frac{dt_r}{dt} - c^2
    \right)
\end{equation}
Since we know $\partial\mathbf v/\partial t$ already, the second term of $\partial\mathbf A/\partial t$ becomes
\begin{equation}
    \frac{V}{c^2}\frac{\partial\mathbf v}{\partial t}
    =
    \frac{qc}{4\pi\epsilon_0} \frac{1}{(\eta c - \spvec \cdot \mathbf v)^2}
    \frac{\mathbf a}{c^2} (\eta c - \spvec \cdot \mathbf v) \frac{\partial t_r}{\partial t}
\end{equation}
Putting them together,
\begin{equation}
    \frac{\partial\mathbf A}{\partial t}
    =
    \frac{qc}{4\pi\epsilon_0} \frac{1}{(\eta c - \spvec \cdot \mathbf v)^2}
    \left[
        \left(
            (c^2 - v^2 + \spvec \cdot \mathbf a)\frac{\mathbf v}{c^2}
            +
            (\eta c - \spvec \cdot \mathbf v)\frac{\mathbf a}{c^2}
        \right) \frac{\partial t_r}{\partial t}
        - \mathbf v
    \right]
\end{equation}
To get $\partial t_r/\partial t$, we differentiate Eq.~(\ref{eq:ch10:RetardedTime:PointCharge:1}).
Evaluating
\[
    \begin{aligned}
        \frac{\partial\eta}{\partial t}
        &=
        \frac{\partial}{\partial t}\sqrt{\spvec \cdot \spvec}
        =
        \frac{1}{\eta} \left(\spvec \cdot \frac{\partial\spvec}{\partial t}\right)
        \\
        &=
        \frac{1}{\eta} \spvec \cdot \left( -\frac{d\mathbf w}{dt_r}\frac{\partial t_r}{\partial t} \right)
        \\
        &=
        -\frac{1}{\eta} (\spvec \cdot \mathbf v) \frac{\partial t_r}{\partial t}
    \end{aligned}
\]
and equating it to $c(1 - \partial t_r/\partial t)$,
\[
    c = c\frac{\partial t_r}{\partial t} - \frac{1}{\eta} (\spvec \cdot \mathbf v) \frac{\partial t_r}{\partial t}
    \quad\rightarrow\quad
    \eta c = (\eta c - \spvec \cdot \mathbf v) \frac{\partial t_r}{\partial t}
\]
Therefore,
\begin{equation}
    \frac{\partial t_r}{\partial t}
    =
    \frac{\eta c}{\eta c - \spvec \cdot \mathbf v}
\end{equation}
Substituting it into $\partial\mathbf A/\partial t$, we arrive at
\begin{equation}
    \begin{aligned}
        \frac{\partial\mathbf A}{\partial t}
        &=
        \frac{qc}{4\pi\epsilon_0}\frac{1}{(\eta c - \spvec \cdot \mathbf v)^3}
        \left[
            \left(
                (c^2 - v^2 + \spvec \cdot \mathbf a) \frac{\mathbf v}{c^2}
                +
                (\eta c - \spvec \cdot \mathbf v) \frac{\mathbf a}{c^2}
            \right) \eta c
            -
            (\eta c - \spvec \cdot \mathbf v)\mathbf v
        \right]
        \\
        &=
        \frac{qc}{4\pi\epsilon_0}\frac{1}{(\eta c - \spvec \cdot \mathbf v)^3}
        \left(
            (\eta c - \spvec \cdot \mathbf v)(-\mathbf v + \frac{\eta}{c}\mathbf a)
            +
            \frac{\eta}{c} (c^2 - v^2 + \spvec \cdot \mathbf a)\mathbf v
        \right)
    \end{aligned}
\end{equation}

Introducing the vector
\begin{equation}
    \mathbf u \equiv c \uspvec - \mathbf v
\end{equation}
the electric field reads
\[
    \begin{aligned}
        \mathbf E
        &=
        -\nabla V - \frac{\partial\mathbf A}{\partial t}
        \\
        &=
        \frac{1}{4\pi\epsilon_0}\frac{qc}{(\underbrace{\eta c - \spvec \cdot \mathbf v}_{\color{blue}\spvec \cdot \mathbf u})^3}
        \left[
            (\eta c - \spvec \cdot \mathbf v)\cancel{\mathbf v}
            -
            (c^2 - v^2 + \spvec \cdot \mathbf a)\spvec
            +
            \underbrace{(\eta c - \spvec \cdot \mathbf v)}_{\color{blue}\spvec \cdot \mathbf u}
            \left(\cancel{-\mathbf v} + \frac{\eta}{c}\mathbf a\right)
        \right.
        \\
        &\qquad\qquad\qquad\qquad\qquad\qquad\qquad +
        \left.
        \frac{\eta}{c} (c^2 - v^2 + \spvec \cdot \mathbf a)\mathbf v
        \right]
        \\
        &=
        \frac{1}{4\pi\epsilon_0}\frac{qc}{(\spvec \cdot \mathbf u)^3}
        \left[
            (c^2 - v^2)\left( \spvec - \frac{\eta}{c}\mathbf v \right)
            +
            (\spvec \cdot \mathbf a)\spvec - \frac{\eta}{c}(\spvec \cdot \mathbf a)\mathbf v
            -
            (\spvec \cdot \mathbf u)\frac{\eta}{c}\mathbf a
        \right]
        \\
        &=
        \frac{1}{4\pi\epsilon_0}\frac{q\eta}{(\spvec \cdot \mathbf u)^3}
        \left[
            (c^2 - v^2)(c\uspvec - \mathbf v)
            +
            \underbrace{(\spvec \cdot \mathbf a)c\uspvec - (\spvec \cdot \mathbf a)\mathbf v}_{\color{blue}(\spvec \cdot \mathbf a)\mathbf u}
            -
            (\spvec \cdot \mathbf u)\mathbf a
        \right]
    \end{aligned}
\]
After making use of the triple vector product rule, $\mathbf \spvec \times (\mathbf u \times \mathbf a) = (\mathbf \spvec \cdot \mathbf a)\mathbf u - (\mathbf \spvec \cdot \mathbf u)\mathbf a$, the electric field reads
\begin{equation}
    \boxed{%
        \mathbf E
        =
        \frac{q}{4\pi\epsilon_0} \frac{\eta}{(\spvec \cdot \mathbf u)^3}
        \left( (c^2 - v^2)\mathbf u + \spvec \times (\mathbf u \times \mathbf a) \right)
    }
    \label{eq:ch10:MovingPointChargeE}
\end{equation}

Meanwhile, the magnetic field is given by
\begin{equation}
    \mathbf B = \nabla \times \mathbf A
    = \frac{1}{c^2} \nabla \times (V \mathbf v)
    = \frac{1}{c^2} \left( V(\nabla \times \mathbf v) - \mathbf v \times \nabla V \right)
\end{equation}
Substituting $\nabla \times \mathbf v$ and $\nabla V$ that we calculated earlier, it follows
\[
    \begin{aligned}
        \mathbf B
        &=
        \frac{1}{c^{\cancel{2}}} \frac{q\cancel{c}}{4\pi\epsilon_0}
        \frac{1}{(\spvec \cdot \mathbf u)^3}
        \left[
            (\spvec \cdot \mathbf u)^{\cancel{2}} \left( -\mathbf a \times \frac{-\spvec}{\cancel{\spvec \cdot \mathbf u}} \right)
            -
            \mathbf v \times \left\{ (\spvec \cdot \mathbf u)\mathbf v - (c^2 - v^2 + \spvec \cdot \mathbf a)\spvec \right\}
        \right]
        \\
        &=
        \frac{1}{c}\frac{q}{4\pi\epsilon_0}\frac{1}{(\spvec \cdot \mathbf u)^3}
        \left(
            -(\spvec \cdot \mathbf u)(\spvec \times \mathbf a)
            -(c^2 - v^2 + \spvec \cdot \mathbf a)(\spvec \times \mathbf v)
        \right)
        \\
        &=
        \frac{1}{c}\frac{q}{4\pi\epsilon_0}\frac{-1}{(\spvec \cdot \mathbf u)^3}
        \spvec \times \left[
            (c^2 - v^2)\mathbf v
            +
            (\spvec \cdot \mathbf a)\mathbf v
            +
            (\spvec \cdot \mathbf a)\mathbf u
        \right]
    \end{aligned}
\]
Making use of $\spvec \times \mathbf u = -\spvec \times \mathbf v$, we can turn $\mathbf B$ into a more symmetric form
\begin{equation}
    \begin{aligned}
        \mathbf B
        &=
        \frac{1}{c}\frac{q}{4\pi\epsilon_0}\frac{1}{(\spvec \cdot \mathbf u)^3}
        \spvec \times
        \left[
            (c^2 - v^2)\mathbf u
            +
            (\spvec \cdot \mathbf a)\mathbf u
            -
            (\spvec \cdot \mathbf a)\mathbf u
        \right]
        \\
        &=
        \frac{1}{c}\frac{q}{4\pi\epsilon_0}\frac{\eta}{(\spvec \cdot \mathbf u)^3}
        \uspvec \times
        \left[
            (c^2 - v^2)\mathbf u
            +
            \spvec \times (\mathbf u \times \mathbf a)
        \right]
    \end{aligned}
\end{equation}
Comparing it with $\mathbf E$, we conclude
\begin{equation}
    \boxed{%
        \mathbf B = \frac{1}{c} \uspvec \times \mathbf E
    }
    \label{eq:ch10:MovingPointChargeB}
\end{equation}
Evidently, the magnetic field of a point charge is always perpendicular to the electric field, and to the vector from the retarding point.
\\

The first term in $\mathbf E$
\[
    \frac{\eta \mathbf u}{(\spvec \cdot \mathbf u)^3}
\]
falls off as the inverse square of the distance from the particle $(\propto \eta^{-2})$.
If $\mathbf v = \mathbf a = 0$, this term alone survives and reduces to
\[
    \mathbf E = \frac{1}{4\pi\epsilon_0} \frac{q}{\eta^2} \uspvec
\]
For this reason, the first term of $\mathbf E$ is called the \textbf{generalized Coulomb's field}.
(Or, since it does not depend on the acceleration, it is also known as the \textbf{velocity field}.)

The second term in $\mathbf E$
\[
    \frac{\spvec \times (\eta \mathbf u \times \mathbf a)}{(\spvec \cdot \mathbf u)^3}
\]
falls off as $\eta^{-1}$ and becomes the dominant term at large distances.
This term is called the \textbf{radiation field} (or \textbf{acceleration field}), since as we will see, it is responsible for electromagnetic radiation.
\\

Finally, the Lorentz force law determines the force on a test charge $Q$:
\begin{equation}
    \begin{aligned}
        \mathbf F
        &=
        Q (\mathbf E + \mathbf V \times \mathbf B)
        \\
        &=
        \frac{qQ}{4\pi\epsilon_0}\frac{\eta}{(\spvec \cdot \mathbf u)^3}
        \left[
            \left( (c^2 - v^2)\mathbf u + \spvec \times (\mathbf u \times \mathbf a) \right)
            \phantom{\frac{\mathbf V}{c}}
        \right.
        \\
        &\qquad\qquad\qquad\qquad\qquad
        \left.
            +
            \frac{\mathbf V}{c} \times
            \left( \spvec \times ((c^2 - v^2)\mathbf u + \spvec \times (\mathbf u \times \mathbf a)) \right)
        \right]
    \end{aligned}
\end{equation}
where $\mathbf V$ is the velocity of $Q$ and $(\spvec, \mathbf u, \mathbf v, \mathbf a)$ are all evaluated at the retarded time.

\framed{%
    \example{10.4}%
    Calculate the electric and magnetic fields of a point charge moving at a constant velocity.
}
\solution%

Again, the particle trajectory is given by
\[
    \mathbf w(t) = \mathbf v t
\]
Substituting $\mathbf a = 0$ into Eq.~(\ref{eq:ch10:MovingPointChargeE}),
\[
    \mathbf E
    =
    \frac{q}{4\pi\epsilon_0}
    \frac{(c^2 - v^2)}{(\spvec \cdot \mathbf u)^3}
    \eta \mathbf u
\]
Expanding $\eta \mathbf u$,
\[
    \begin{aligned}
        \eta\mathbf u
        &=
        c\spvec - \eta\mathbf v
        \\
        &=
        c(\mathbf r - \mathbf v t_r) - c(t - t_r)\mathbf v
        \\
        &=
        c(\mathbf r - \mathbf v t)
    \end{aligned}
\]
In Ex. 10.3, we found that
\[
    \spvec \cdot \mathbf u
    =
    \eta c - \spvec \cdot \mathbf v
    =
    R\sqrt{c^2 - v^2 \sin^2\theta}
\]
where $\mathbf R \equiv \mathbf r - \mathbf v t$ is the vector from the present location of the particle to $\mathbf r$, and $\theta$ is the angle between $\mathbf R$ and $\mathbf v$.
Putting all things together,
\[
    \begin{aligned}
        \mathbf E
        &=
        \frac{q}{4\pi\epsilon_0} \frac{c^2 - v^2}{R^3 (c^2 - v^2\sin^2\theta)^{3/2}} \mathbf R c
        \\
        &=
        \frac{q}{4\pi\epsilon_0} \frac{1 - v^2/c^2}{1 - v^2\sin^2\theta/c^2} \frac{\uvec R}{R^2}
    \end{aligned}
\]

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch10-example_4-01}
    \caption{Example 10.4 Electric field generated by a moving charge.}%
\end{figure}
Interestingly, its direction is parallel to $\mathbf R$, determined by the \textit{present} position of the particle.
The magnitude becomes minimum when $\sin^2\theta = 0$ (i.e., $\theta = 0, \pi$) whose value is reduced by a factor $(1 - v^2/c^2)$ relative to the field of a charge at rest; and maximum when $\sin^2\theta = 1$ (i.e., $\theta = \pi/2, 3\pi/2$) whose value is enhanced by a factor $1/\sqrt{1 - v^2/c^2}$.
\\

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch10-example_4-02}
    \caption{Example 10.4 Magnetic field generated by a moving charge.}%
\end{figure}
As for $\mathbf B$, we have
\[
    \uspvec = \frac{\mathbf r - \mathbf v t_r}{\eta}
    = \frac{(\mathbf r - \mathbf v t) + (\mathbf v t - \mathbf v t_r)}{\eta}
    = \frac{\mathbf R}{\eta} + \frac{\mathbf v}{c}
\]
and therefore
\[
    \mathbf B = \frac{1}{c} (\uspvec \times \mathbf E)
    = \frac{1}{c^2} (\mathbf v \times \mathbf E)
\]
If we choose the $z$ axis along $\mathbf v$, then
\[
    \mathbf v \times \mathbf E
    =
    vE \sin\psi \uvec \phi
\]
where $\psi$ is the angle between $\mathbf v$ and $\mathbf E$ and $\uvec\phi$ is the unit vector in the azimuthal direction.
Both $\mathbf E$ and $\mathbf B$ are azimuthally symmetric.
\\

When $v^2 \ll c^2$, they reduce to
\[
    \mathbf E(\mathbf r, t)
    \simeq
    \frac{1}{4\pi\epsilon_0} \frac{q}{R^2} \uvec R
    ,\quad
    \mathbf B(\mathbf r, t)
    \simeq
    \frac{\mu_0}{4\pi} \frac{q\mathbf v \times \uvec R}{R^2}
\]
The first is Coulomb's law, and the second is ``Biot-Savart law for a point charge''.

\framed{%
    Homework problems: 10.20, 10.21, 10.23(1st part)
}
