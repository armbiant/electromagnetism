#!/bin/sh
#
# Copyright (c) 2023, Kyungguk Min
#
# SPDX-License-Identifier: BSD-2-Clause
#

# Set site directory
SITE_DIR="$1"
if [ -z "${SITE_DIR}" ]; then
    SITE_DIR=public
fi
echo "Site Directory : ${SITE_DIR}"

# Build the site
mkdocs build --strict --verbose --config-file mkdocs.yml --site-dir "${SITE_DIR}" || exit 1

# Build LaTeX book; the second command is to remove all generated files except .pdf
make -C src install INSTALL_PREFIX="$PWD/$SITE_DIR" || exit 1
